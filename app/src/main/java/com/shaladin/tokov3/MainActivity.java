package com.shaladin.tokov3;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import com.shaladin.tokov3.activity.CartActivity;
import com.shaladin.tokov3.activity.LoginActivity;
import com.shaladin.tokov3.config.Variable;
import com.shaladin.tokov3.database.UserAuth;
import com.shaladin.tokov3.fragment.HomeFragment;
import com.shaladin.tokov3.fragment.ProfileFragment;
import com.shaladin.tokov3.fragment.TransactionsFragment;
import com.shaladin.tokov3.model.Auth;
import com.shaladin.tokov3.utils.Misc;

public class MainActivity extends AppCompatActivity {
//    SharedPreferences UserPrefs;

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            selectFragment(item);
            return false;
        }

    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //Check User Authentication
//        UserPrefs   = getSharedPreferences(Variable.USER_PREFERENCE, Context.MODE_PRIVATE);
//        userAuth    = new UserAuth(this);
//        userAuth.open();
//        auth        = userAuth.isAuth();
//
        setupNavigationView();
    }

    private void setupNavigationView() {
        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        if(navigation != null) {
            Menu menu = navigation.getMenu();
            selectFragment(menu.getItem(0));
            navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        }
    }

    /**
     * Perform when any action item is selected
     */
    protected void selectFragment(MenuItem item) {
        item.setChecked(true);
        Bundle arguments;
        switch (item.getItemId()) {
            case R.id.navigation_home :
                pushFragment(new HomeFragment());
                break;

            case R.id.navigation_transactions :
                pushFragment(new TransactionsFragment());
                break;

            case R.id.navigation_profile :
                if(Misc.checkAuth(this))
                {
                    Fragment profileFragment = new ProfileFragment();
                    arguments = new Bundle();
                    arguments.putString("param1", "Item1");
                    arguments.putString("param2", "Item2");
                    profileFragment.setArguments(arguments);
                    pushFragment(profileFragment);
                }
                else
                {
                    loginFirst();
                }
                break;
        }
    }
    /**
     * Method to push any fragment into given id
     */
    protected void pushFragment(Fragment fragment){
        if (fragment == null)
            return;

        FragmentManager fragmentManager = getSupportFragmentManager();
        if(fragmentManager != null) {
            FragmentTransaction ft = fragmentManager.beginTransaction();
            if(ft != null) {
                ft.replace(R.id.content, fragment);
                ft.commit();
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //Inflate the menu
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch (id) {
            case R.id.actionCart :
                Intent Cart = new Intent(this, CartActivity.class);
                startActivity(Cart);
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    private void loginFirst() {
        Intent Login = new Intent(this, LoginActivity.class);
        Login.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        startActivity(Login);
    }

}
