package com.shaladin.tokov3;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Handler;

import com.shaladin.tokov3.config.ApiServices;
import com.shaladin.tokov3.config.Company;
import com.shaladin.tokov3.config.Variable;
import com.shaladin.tokov3.http.Requests;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by EliteBook on 5/17/2017.
 */

public class MyApp extends Application {

    final Handler productHandler  = new Handler();
    SharedPreferences productsPrefs;
    SharedPreferences statesPrefs;
    SharedPreferences paymentPrefs;

    @Override
    public void onCreate() {
        super.onCreate();

        productsPrefs = getSharedPreferences(Variable.PRODUCTS_PREFERENCE, Context.MODE_PRIVATE);
        statesPrefs   = getSharedPreferences(Variable.STATES_PREFERENCE, Context.MODE_PRIVATE);
        paymentPrefs  = getSharedPreferences(Variable.PAYMENT_PREFERENCE, Context.MODE_PRIVATE);

        updateProducts updateProducts = new updateProducts();
        productHandler.postDelayed(updateProducts, 10000);

        //Get States
        if(!statesPrefs.contains(Variable.STATE)) new StatesAsyncTask().execute(ApiServices.PROVINSI);

        //Get Payments
        updatePayments();
    }

    private void requestUpdateProducts() {
        String url = "http://api.grosirbersama.co.id/api/products?items_per_page=10&company_id=" + Company.COMPANY_ID;
        new ProductsAsyncTask().execute(url);
    }

    private void updatePayments() {
        String companyId;
        if(!paymentPrefs.contains(Variable.PAYMENT))
        {
            new PaymentsAsyncTask().execute();
        }
        else {
            companyId = paymentPrefs.getString(Variable.COMPANY_ID, null);
            if(!companyId.equals(Company.COMPANY_ID))
                new PaymentsAsyncTask().execute();
        }
    }

    private class StatesAsyncTask extends AsyncTask<String, Void, JSONObject> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected JSONObject doInBackground(String... params) {
            try {
                JSONObject jsonObject = Requests.getData(params[0]);
                return jsonObject;
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(JSONObject jsonObject) {
            if(jsonObject != null)
            {
                boolean isError;
                try
                {
                    isError = jsonObject.getBoolean(Variable.ERROR);

                    if(!isError) {
                        JSONArray dataStates = jsonObject.getJSONArray(Variable.RESPONSE);

                        SharedPreferences.Editor editorStates = statesPrefs.edit();
                        editorStates.putString(Variable.STATE, dataStates.toString());
                        editorStates.commit();
                    }
                }
                catch (JSONException je)
                {
                    je.printStackTrace();
                }
            }
        }
    }

    private class PaymentsAsyncTask extends AsyncTask<String, Void, JSONObject> {
        @Override
        protected JSONObject doInBackground(String... params) {
            try {
                JSONObject json = Requests.getData(ApiServices.PAYMENT + Company.COMPANY_ID);
                return json;
            } catch (Exception e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(JSONObject json) {
            if(json != null) {
                try {
                    JSONArray payments = json.getJSONArray(Variable.RESPONSE);

                    SharedPreferences.Editor editorPayments = paymentPrefs.edit();
                    editorPayments.putString(Variable.COMPANY_ID, Company.COMPANY_ID);
                    editorPayments.putString(Variable.PAYMENT, payments.toString());
                    editorPayments.commit();

                } catch (JSONException je) {
                    je.printStackTrace();
                }
            }
        }
    }

    private class ProductsAsyncTask extends AsyncTask<String, Void, JSONObject> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected JSONObject doInBackground(String... params) {
            try {
                String url = params[0];
                JSONObject jsonObject = Requests.getData(url);
                return jsonObject;
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(JSONObject jsonObject) {
            if(jsonObject != null)
            {
                SharedPreferences.Editor editorProductsPrefs = productsPrefs.edit();
                editorProductsPrefs.putString(Variable.PRODUCTS, jsonObject.toString());
                editorProductsPrefs.commit();
            }
        }
    }

    private class updateProducts implements Runnable {
        int i = 0;
        public void run() {
            requestUpdateProducts();
            productHandler.postDelayed(this, 300000);
//            i = i+1;
//            Toast.makeText(MyApp.this, "Updated...", Toast.LENGTH_SHORT).show();
//            System.out.println(i);
        }
    }

}
