package com.shaladin.tokov3.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.shaladin.tokov3.R;
import com.shaladin.tokov3.activity.checkout.CheckoutShipping;
import com.shaladin.tokov3.adapter.CartAdapter;
import com.shaladin.tokov3.database.Carts;
import com.shaladin.tokov3.database.UserAuth;
import com.shaladin.tokov3.model.Auth;
import com.shaladin.tokov3.model.Cart;
import com.shaladin.tokov3.utils.DividerItemDecoration;
import com.shaladin.tokov3.utils.Misc;

import java.text.DecimalFormat;
import java.util.List;

public class CartActivity extends AppCompatActivity implements View.OnClickListener {
    private List<Cart> carts;
    private CartAdapter cartAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cart);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        updateSubtotal();
        Button actionCheckout = (Button) findViewById(R.id.btnCheckout);
        actionCheckout.setOnClickListener( this );

        if(carts == null)
            actionCheckout.setText("Kembali Belanja");
        else
            initView(carts);

    }

    public void initView(List<Cart> carts) {
        RecyclerView cartCycle = (RecyclerView) findViewById(R.id.CartRecycle);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        cartCycle.setLayoutManager(layoutManager);
        cartCycle.setHasFixedSize(true);
//        cartCycle.setNestedScrollingEnabled(false);
        cartAdapter = new CartAdapter(this, carts);
        cartCycle.setAdapter(cartAdapter);
        //Add Item Decoration
        RecyclerView.ItemDecoration itemDecoration = new DividerItemDecoration(this, DividerItemDecoration.VERTICAL_LIST);
        cartCycle.addItemDecoration(itemDecoration);

        if(cartCycle.getChildCount() == carts.size()) {
            layoutManager.setStackFromEnd(true);
        } else  {
            layoutManager.setStackFromEnd(false);
        }

        cartAdapter.setOnDataChangeListener(new CartAdapter.OnDataChangeListener() {
            @Override
            public void onDataChanged(int size) {
                updateSubtotal();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //Inflate the menu
        return  true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch (id) {
            case android.R.id.home :
                super.onBackPressed();
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    private void updateSubtotal() {
        double sumSubtotal = 0;

        Carts c = new Carts(this);
        c.open();
        carts = c.allCarts();
        if(carts != null) {
            for (Cart cart : carts) {
                sumSubtotal = sumSubtotal + cart.getSubtotal();
            }
        }

        DecimalFormat formatter = new DecimalFormat("#,###");
        TextView SubTotal = (TextView) findViewById(R.id.sumSubTotal);
        SubTotal.setText("Rp. " + formatter.format(sumSubtotal));
    }

    @Override
    public void onClick(View view) {
        //Get View Id
        int id = view.getId();

        switch (id) {
            case R.id.btnCheckout:
                if(carts != null) {
                    if(Misc.checkAuth(this))
                    {
                        Intent shipping = new Intent(this, CheckoutShipping.class);
                        startActivity(shipping);
                    }
                    else
                    {
                        Intent Login = new Intent(this, LoginActivity.class);
                        Login.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                        startActivity(Login);
                    }
                }
                else {
                    super.onBackPressed();
                }
                break;
        }
    }
}