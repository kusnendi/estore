package com.shaladin.tokov3.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import com.shaladin.tokov3.R;
import com.shaladin.tokov3.config.ApiServices;
import com.shaladin.tokov3.config.Company;
import com.shaladin.tokov3.config.Variable;
import com.shaladin.tokov3.model.User;
import com.shaladin.tokov3.http.Requests;
import com.shaladin.tokov3.utils.Misc;

import org.json.JSONException;
import org.json.JSONObject;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {

    private Context context;
    private String b64Password;

    SharedPreferences UserPrefs;
    EditText inputEmail;
    EditText inputPassword;
    View container;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        UserPrefs = getSharedPreferences(Variable.USER_PREFERENCE, Context.MODE_PRIVATE);
        initView();
    }

    public void initView() {
        context         = this;
        inputEmail      = (EditText) findViewById(R.id.inputEmail);
        inputPassword   = (EditText) findViewById(R.id.inputPassword);
        container       = findViewById(R.id.container);

        ImageButton doClose  = (ImageButton) findViewById(R.id.btnClose);
        doClose.setOnClickListener(this);

        Button doLogin  = (Button) findViewById(R.id.btnLogin);
        doLogin.setOnClickListener(this);

        TextView RegisterNow    = (TextView) findViewById(R.id.linkRegister);
        RegisterNow.setOnClickListener(this);

        TextView ForgetPassword = (TextView) findViewById(R.id.forgetPassword);
        ForgetPassword.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();

        switch (id) {
            case R.id.btnClose:
                super.onBackPressed();
                break;

            case R.id.btnLogin:
                String Email    = String.valueOf(inputEmail.getText());
                String Password = String.valueOf(inputPassword.getText());
                String B64Password = Misc.encodeBase64(Password);

                if(Validator()) {
                    b64Password    = B64Password;
                    String authUrl = ApiServices.AUTH + "?email=" + Email + "&password=" +
                            B64Password + "&company_id=" + Company.COMPANY_ID;
                    //Requests Auth Service
                    new LoginAsyncTask().execute(authUrl.replaceAll("\\n", ""));
                }
                break;

            case R.id.linkRegister:
                Intent Register = new Intent(this, RegisterActivity.class);
                startActivity(Register);
                break;

            case R.id.forgetPassword:
                //
                break;
        }
    }

    private class LoginAsyncTask extends AsyncTask<String, Void, JSONObject> {
        ProgressDialog dialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            dialog = new ProgressDialog(context);
            dialog.setMessage("Authenticating...");
            dialog.show();
        }

        @Override
        protected JSONObject doInBackground(String... params) {
            try {
                JSONObject jsonObject = Requests.getData(params[0]);
                return jsonObject;
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(JSONObject jsonObject) {
            dialog.dismiss();

            if(jsonObject != null)
            {
                boolean isAuth; String secretKey; String isActive;
                JSONObject data; String message;

                try
                {
                    isAuth = jsonObject.getBoolean(Variable.isAUTH);
                    if(isAuth)
                    {
                        data     = jsonObject.getJSONObject(Variable.DATA);
                        isActive = data.getString(Variable.STATUS);
                        if(isActive.equals("A"))
                        {
                            secretKey           = jsonObject.getString(Variable.KEY);
                            int userId          = Integer.parseInt(data.getString(Variable.USER_ID));
                            String firstName    = data.getString(Variable.FIRST_NAME);
                            String lastName     = data.getString(Variable.LAST_NAME);
                            String city         = data.getString("kabupaten_name");
                            String district     = data.getString("city_name");
                            String email        = data.getString(Variable.EMAIL);
                            String address      = data.getString(Variable.ADDRESS);
                            String phone        = data.getString(Variable.PHONE);
                            String postCode     = data.getString(Variable.ZIP);

                            User user = new User(userId, firstName, lastName, city, address, phone, email, postCode);
                            boolean saveAuth = storeCredential(user, b64Password, secretKey);
                            if(saveAuth) finish();
                        }
                        else
                        {
                            message = "Maaf akun anda belum di aktivasi, silakan hubungi admin toko!";
                            Snackbar.make(container, message, Snackbar.LENGTH_LONG).show();
                        }
                    }
                    else {
                        message = jsonObject.getString(Variable.MESSAGE);
                        Snackbar.make(container, message, Snackbar.LENGTH_LONG).show();
                    }
                }
                catch (JSONException e) {
                    Log.e(Variable.ERROR, e.getLocalizedMessage());
                }
            }
            else
            {
                Snackbar.make(container, "Connection Failed!", Snackbar.LENGTH_LONG).show();
            }
        }

    }

    private boolean storeCredential(User user, String b64Password, String secretKey) {
        //Edit User Preferences
        SharedPreferences.Editor editUserPrefs = UserPrefs.edit();
        editUserPrefs.putInt(Variable.USER_ID, user.getUserId());
        editUserPrefs.putString(Variable.FIRST_NAME, user.getFirstName());
        editUserPrefs.putString(Variable.LAST_NAME, user.getLastName());
        editUserPrefs.putString(Variable.CITY, user.getCityName());
        editUserPrefs.putString(Variable.ADDRESS, user.getAddress());
        editUserPrefs.putString(Variable.ZIP, user.getPostCode());
        editUserPrefs.putString(Variable.PHONE, user.getPhone());
        editUserPrefs.putString(Variable.EMAIL, user.getEmail());
        editUserPrefs.putString(Variable.KEY, secretKey);
        editUserPrefs.putString(Variable.PASSWORD, b64Password);
        editUserPrefs.putBoolean(Variable.IS_AUTH, true);
        editUserPrefs.commit();

        return true;
    }

    private boolean Validator() {
        boolean isValid = true;

        EditText Username = (EditText) findViewById(R.id.inputEmail);
        String Email = String.valueOf(Username.getText());

        TextInputLayout layoutEmail = (TextInputLayout) findViewById(R.id.userEmail);
        TextInputLayout layoutPassword = (TextInputLayout) findViewById(R.id.userPassword);

        //Reset Alert
        layoutEmail.setErrorEnabled(false);
        layoutPassword.setErrorEnabled(false);
        if (Email.isEmpty()) {
            layoutEmail.setErrorEnabled(true);
            layoutEmail.setError("Email Required");
            isValid = false;
        }
        else if(!Misc.isValidEmail(Email)) {
            layoutEmail.setErrorEnabled(true);
            layoutEmail.setError("Invalid Email");
            isValid = false;
        }

        EditText inputPassword = (EditText) findViewById(R.id.inputPassword);
        String Password = String.valueOf(inputPassword.getText());

        if(Password.isEmpty())
        {
            layoutPassword.setErrorEnabled(true);
            layoutPassword.setError("Password Required");
            isValid = false;
        }
        else if(Password.length() < 6) {
            layoutPassword.setErrorEnabled(true);
            layoutPassword.setError("Password Min 6 Char");
            isValid = false;
        }

        return isValid;
    }
}