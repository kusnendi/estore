package com.shaladin.tokov3.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.shaladin.tokov3.R;
import com.shaladin.tokov3.config.ApiServices;
import com.shaladin.tokov3.config.Company;
import com.shaladin.tokov3.config.Variable;
import com.shaladin.tokov3.http.Requests;
import com.shaladin.tokov3.utils.Misc;

import org.json.JSONException;
import org.json.JSONObject;

public class RegisterActivity extends AppCompatActivity implements View.OnClickListener {

    private String Fullname;
    private String Email;
    private String Phone;
    private String Password;
    private int CompanyId;

    EditText iFullname;
    EditText iEmail;
    EditText iPhone;
    EditText iPassword;
    View Container;
    Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        CompanyId = Integer.parseInt(Company.COMPANY_ID);
        //Initial view
        initView();
    }

    private void initView() {
        context   = this;
        Container = findViewById(R.id.content);
        iFullname = (EditText) findViewById(R.id.inputName);
        iEmail    = (EditText) findViewById(R.id.inputEmail);
        iPhone    = (EditText) findViewById(R.id.inputTelepon);
        iPassword = (EditText) findViewById(R.id.inputPassword);

        Button Register = (Button) findViewById(R.id.btnRegister);
        Register.setOnClickListener(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch (id) {
            case android.R.id.home:
                super.onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private boolean Validation() {
        boolean isValid = true;
        Fullname = String.valueOf(iFullname.getText());
        Email    = String.valueOf(iEmail.getText());
        Password = Misc.encodeBase64(String.valueOf(iPassword.getText()));
        Phone    = String.valueOf(iPhone.getText());

        if(Fullname.isEmpty()) {
            iFullname.setError("Nama Lengkap Harus di Isi");
            iFullname.requestFocus();
            isValid = false;
        } else if (Phone.isEmpty()) {
            iPhone.setError("Nomor Telepon Harus di Isi");
            iPhone.requestFocus();
            isValid = false;
        } else if(Email.isEmpty()) {
            iEmail.setError("Email Harus di Isi");
            iEmail.requestFocus();
            isValid = false;
        } else if(!Misc.isValidEmail(Email)) {
            iEmail.setError("Alamat Email Tidak Valid");
            iEmail.requestFocus();
            isValid = false;
        } else if(Password.isEmpty()) {
            iPassword.setError("Password Harus di Isi");
            iPassword.requestFocus();
            isValid = false;
        } else if (Password.length() < 6) {
            iPassword.setError("Password Minimal 6 karakter!");
            iPassword.requestFocus();
            isValid = false;
        }

        return isValid;
    }

    private String DataJSON() {
        try {
            JSONObject json = new JSONObject();
            json.put(Variable.FULLNAME, Fullname);
            json.put(Variable.EMAIL, Email);
            json.put(Variable.PHONE, Phone);
            json.put(Variable.PASSWORD, Password);
            json.put(Variable.COMPANY_ID, CompanyId);
            json.put(Variable.GROUP_ID, 19);

            return json.toString();
        } catch (JSONException e) {
            return null;
        }
    }

    private class RegisterAsyncTask extends AsyncTask<String, Void, String> {
        ProgressDialog dialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            dialog = new ProgressDialog(context);
            dialog.setMessage("Mohon tunggu...");
            dialog.show();
        }
        @Override
        protected String doInBackground(String... params) {
            try {
                String json = Requests.postData(params[0], params[1]);
                return json;
            } catch (Exception e) {
                Log.i("Error", e.getLocalizedMessage());
            }
            return null;
        }
        @Override
        protected void onPostExecute(String result) {
            dialog.dismiss();

            if(result != null) {

                try
                {
                    JSONObject json = new JSONObject(result);
                    boolean isError = json.getBoolean(Variable.ERROR);
                    String message;

                    if(isError) {
                        message = json.getString(Variable.RESPONSE);
                        Snackbar.make(Container, message, Snackbar.LENGTH_LONG).show();
                    }
                    else {
                        message = "Pendaftaran Berhasil, Silahkan Login dengan Email dan Password Anda";
                        Snackbar.make(Container, message, Snackbar.LENGTH_LONG).show();
                        System.out.println(result);
                        goBack();
                    }
                }
                catch (JSONException e) {
                    Log.i(Variable.ERROR, e.getLocalizedMessage());
                    Snackbar.make(Container, e.getLocalizedMessage(), Snackbar.LENGTH_LONG).show();
                }

            } else {
                Snackbar.make(Container, "Connection Failed!", Snackbar.LENGTH_LONG).show();
            }
        }
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();

        switch (id) {
            case R.id.btnRegister:
                if(Validation()) {
                    //Requests Register Account Service
                    new RegisterAsyncTask().execute(ApiServices.CUSTOMER, DataJSON());
                }
                break;
        }
    }

    private void goBack() {
        super.onBackPressed();
    }
}