package com.shaladin.tokov3.activity.checkout;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.shaladin.tokov3.R;
import com.shaladin.tokov3.config.Variable;
import com.shaladin.tokov3.database.Carts;
import com.shaladin.tokov3.model.Cart;
import com.shaladin.tokov3.model.Payment;
import com.shaladin.tokov3.model.Product;
import com.shaladin.tokov3.utils.Formatter;
import com.shaladin.tokov3.utils.Misc;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;

public class CheckoutPayment extends AppCompatActivity implements View.OnClickListener {

    private List<Payment> paymentList;
    private int paymentId;
    private double weight;
    private String notes;

    private SharedPreferences paymentsPrefs;
    private SharedPreferences cartPrefs;
    private SharedPreferences userPrefs;

    TextView txtSubtotal;
    TextView txtShippingCost;
    TextView txtTotal;
    EditText checkoutNotes;
    Button btnSubmit;
    View container;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_checkout_payment);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        paymentsPrefs = getSharedPreferences(Variable.PAYMENT_PREFERENCE, Context.MODE_PRIVATE);
        cartPrefs     = getSharedPreferences(Variable.CART_PREFS, Context.MODE_PRIVATE);
        userPrefs     = getSharedPreferences(Variable.USER_PREFERENCE, Context.MODE_PRIVATE);

        initView();
        setPaymentList();
    }

    private void initView() {
        container = findViewById(R.id.container);

        txtSubtotal     = (TextView) findViewById(R.id.subTotal);
        txtShippingCost = (TextView) findViewById(R.id.shippingCost);
        txtTotal        = (TextView) findViewById(R.id.total);

        checkoutNotes   = (EditText) findViewById(R.id.checkoutNotes);
        btnSubmit       = (Button) findViewById(R.id.btnSubmit);
        btnSubmit.setOnClickListener(this);

        double subtotal = Double.parseDouble(cartPrefs.getString(Variable.SUBTOTAL, null));
        double shipcost = Double.parseDouble(cartPrefs.getString(Variable.SHIPPING_COST, null));
        double total    = Double.parseDouble(cartPrefs.getString(Variable.TOTAL, null));

        //setup value
        txtSubtotal.setText(Formatter.accounting(String.valueOf(subtotal), "Rp"));
        txtShippingCost.setText(Formatter.accounting(String.valueOf(shipcost), "Rp"));
        txtTotal.setText(Formatter.accounting(String.valueOf(total), "Rp"));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private double sumWeight() {
        weight = 0;

        return weight;
    }

    private void setPaymentList() {
        //reset payment list
        paymentList = new ArrayList<>();

        //Check Payments in shared prefs
        if(paymentsPrefs.contains(Variable.PAYMENT))
        {
            String text = paymentsPrefs.getString(Variable.PAYMENT, null);

            try {
                JSONArray arrays = new JSONArray(text);
                if(arrays.length() > 0)
                {
                    for (int i = 0; i < arrays.length(); i++)
                    {
                        JSONObject payment = arrays.getJSONObject(i);

                        Payment obj = new Payment();
                        obj.setId(i);
                        obj.setPaymentId(payment.getInt(Variable.PAYMENT_ID));
                        obj.setPayment(payment.getString("payment_name"));
                        obj.setInstructions(payment.getString(Variable.INSTRUCTIONS));

                        paymentList.add(obj);
                    }

                    initPaymentOptions();
                }

            } catch (JSONException je) {
                je.printStackTrace();
            }
        }
    }

    private void initPaymentOptions() {
        RadioGroup radioGroup = new RadioGroup(this);
        radioGroup.setOrientation(LinearLayout.VERTICAL);
        for (final Payment payment : paymentList) {
            RadioButton rb = new RadioButton(this);
            rb.setId(payment.getPaymentId());
            rb.setText(payment.getPayment());
            rb.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    paymentId = payment.getPaymentId();
                }
            });
            radioGroup.addView(rb);
        }

        ((ViewGroup) findViewById(R.id.payments)).addView(radioGroup);
    }

    private boolean checkPayment() {

        if (paymentId == 0) {
            return false;
        }

        String payment = "";
        for(Payment p : paymentList) {
            if(p.getPaymentId() == paymentId) {
                payment = p.getPayment();
            }
        }

        SharedPreferences.Editor cartEditor = cartPrefs.edit();
        cartEditor.putString(Variable.PAYMENT, payment);
        cartEditor.putInt(Variable.PAYMENT_ID, paymentId);
        cartEditor.commit();

        return true;
    }

    private void placeOrder() {
        notes = "";
        notes = String.valueOf(checkoutNotes.getText());

        try //prepare json data
        {
            List<Cart> items = new ArrayList<>();

            JSONObject products = new JSONObject();
            Carts item  = new Carts(this);
            item.open();
            items       = item.allCarts();
            for (Cart i : items)
            {
                JSONObject product = new JSONObject();
                product.put(Variable.PRODUCT_ID, i.getProductId());
                product.put(Variable.AMOUNT, i.getAmount());
                product.put(Variable.NOTES, i.getNote());
                products.put(String.valueOf(i.getProductId()), product);
            }

            JSONObject cart = new JSONObject();
            cart.put(Variable.USER_ID, cartPrefs.getInt(Variable.USER_ID, 0));
            cart.put(Variable.SHIPPING_ID, cartPrefs.getInt(Variable.SHIPPING_ID, 0));
            cart.put(Variable.SHIPPING_ID+"s", cartPrefs.getInt(Variable.SHIPPING_ID, 0));
            cart.put(Variable.PAYMENT_ID, cartPrefs.getInt(Variable.PAYMENT_ID, 0));
            cart.put(Variable.PRODUCTS, products.toString());
            cart.put("b_"+Variable.FIRST_NAME, cartPrefs.getString(Variable.FIRST_NAME, null));
            cart.put("b_"+Variable.LAST_NAME, cartPrefs.getString(Variable.LAST_NAME, null));
            cart.put("b_"+Variable.ADDRESS, cartPrefs.getString(Variable.ADDRESS, null));
            cart.put("b_"+Variable.STATE, cartPrefs.getInt(Variable.STATE_ID, 0));
            cart.put("b_kabupaten", cartPrefs.getInt(Variable.CITY_ID, 0));
            cart.put("b_city", cartPrefs.getInt(Variable.DISTRICT_ID, 0));
            cart.put("b_zipcode", cartPrefs.getString(Variable.ZIP, null));
            cart.put("b_phone", cartPrefs.getString(Variable.PHONE, null));
            cart.put(Variable.EMAIL, userPrefs.getString(Variable.EMAIL, null));
            cart.put("s_"+Variable.FIRST_NAME, cartPrefs.getString(Variable.FIRST_NAME, null));
            cart.put("s_"+Variable.LAST_NAME, cartPrefs.getString(Variable.LAST_NAME, null));
            cart.put("s_"+Variable.ADDRESS, cartPrefs.getString(Variable.ADDRESS, null));
            cart.put("s_"+Variable.STATE, cartPrefs.getInt(Variable.STATE_ID, 0));
            cart.put("s_kabupaten", cartPrefs.getInt(Variable.CITY_ID, 0));
            cart.put("s_city", cartPrefs.getInt(Variable.DISTRICT_ID, 0));
            cart.put("s_zipcode", cartPrefs.getString(Variable.ZIP, null));
            cart.put("s_phone", cartPrefs.getString(Variable.PHONE, null));
            cart.put("notes", notes);

            System.out.println(cart.toString());
        }
        catch (JSONException je) {
            je.printStackTrace();
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnSubmit:
                //check selected payment
                if(checkPayment()) placeOrder();
                break;

        }
    }
}