package com.shaladin.tokov3.activity.checkout;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;

import com.shaladin.tokov3.R;
import com.shaladin.tokov3.activity.profile.address.AddressNew;
import com.shaladin.tokov3.config.ApiServices;
import com.shaladin.tokov3.config.Variable;
import com.shaladin.tokov3.database.Alamat;
import com.shaladin.tokov3.database.Carts;
import com.shaladin.tokov3.http.Requests;
import com.shaladin.tokov3.model.Address;
import com.shaladin.tokov3.model.Cart;
import com.shaladin.tokov3.utils.Formatter;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class CheckoutShipping extends AppCompatActivity implements AdapterView.OnItemSelectedListener, View.OnClickListener {
    private List<Address> addressList   = new ArrayList<>();
    private List<String> addresses      = new ArrayList<>();
    private List<Cart> listCart         = new ArrayList<>();
    private HashMap<String, Address> mapAddress = new HashMap<>();
    private ArrayAdapter<String> addressAdapter;
    private Alamat alamat;
    private Address billAddress;
    private int userId;
    private int primary;
    private int shippingId;
    private double weight;
    private double shipCost;
    private double subTotal;
    private double total;

    SharedPreferences userPrefs;
    SharedPreferences cartPrefs;
    Spinner spinAddress;
    TextView txtAsName;
    TextView txtStreetAddress;
    TextView txtDistrictCity;
    TextView txtStatePost;
    TextView txtPhone;
    TextView txtSubTotal;
    TextView txtShipCost;
    TextView txtTotal;
    RadioGroup rShipping;
    RadioButton jneYes;
    RadioButton jneReg;
    Button  btnSubmit;
    View container;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_checkout_shipping);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        cartPrefs   = getSharedPreferences(Variable.CART_PREFS, Context.MODE_PRIVATE);
        userPrefs   = getSharedPreferences(Variable.USER_PREFERENCE, Context.MODE_PRIVATE);
        userId      = userPrefs.getInt(Variable.USER_ID, 0);

        alamat = new Alamat(this);
        alamat.open();
        addressList = alamat.getByUserId(userId);
        alamat.close();

        int i = 0;
        for (Address address : addressList) {
            if(address.getPrimary() > 0) primary = i;
            String text = address.getRealName() + " (" + address.getName() + ") " + address.getStreetAddress();

            mapAddress.put(text, address);
            addresses.add(text);
            i++;
        }
        addresses.add("Alamat Baru");

        sumWeight(); sumSubTotal(); shippingId = 0;
        initView();
    }

    private void initView() {
        container   = findViewById(R.id.container);

        txtAsName   = (TextView) findViewById(R.id.txtAsName);
        txtStreetAddress = (TextView) findViewById(R.id.txtStreetAddress);
        txtDistrictCity  = (TextView) findViewById(R.id.txtDistrictCity);
        txtStatePost     = (TextView) findViewById(R.id.txtStatePost);
        txtPhone         = (TextView) findViewById(R.id.txtPhone);
        txtSubTotal      = (TextView) findViewById(R.id.subTotal);
        txtShipCost      = (TextView) findViewById(R.id.shippingCost);
        txtTotal         = (TextView) findViewById(R.id.total);

        rShipping        = (RadioGroup) findViewById(R.id.shipping);
        jneReg           = (RadioButton) findViewById(R.id.jneReg);
        jneYes           = (RadioButton) findViewById(R.id.jneYes);

        jneReg.setOnClickListener( this );
        jneYes.setOnClickListener( this );

        spinAddress      = (Spinner) findViewById(R.id.spinAddress);
        addressAdapter   = new ArrayAdapter<String>(this, R.layout.item_spinner, addresses);
        addressAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinAddress.setAdapter(addressAdapter);
        spinAddress.setOnItemSelectedListener(this);
        spinAddress.setSelection(primary);

        btnSubmit  = (Button) findViewById(R.id.btnSubmit);
        btnSubmit.setOnClickListener(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                break;

            //
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
        switch (adapterView.getId()) {
            case R.id.spinAddress:
                String text = adapterView.getItemAtPosition(position).toString();

                if(addresses.size() == position + 1)
                {
                    Intent newAddress= new Intent(this, AddressNew.class);
                    newAddress.putExtra("action", "create");
                    newAddress.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                    startActivity(newAddress);

                    spinAddress.setSelection(position-1);
                }
                else
                {
                    billAddress = mapAddress.get(text);
                    rShipping.clearCheck();

                    shipCost = 0; sumTotal(subTotal, shipCost);
                    updateCartSummary();
                    updateDisplayAddress(billAddress);
                }
                break;
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }

    private void updateDisplayAddress(Address address) {
        //Update view!
        txtAsName.setText(address.getRealName());
        txtStreetAddress.setText(address.getStreetAddress());
        txtDistrictCity.setText(address.getDistrictName() + ", " + address.getCityName().replaceAll("Kota ", ""));
        txtStatePost.setText(address.getStateName() + " - " + address.getPostCode());
        txtPhone.setText(getString(R.string.input_telepon) + ": " + address.getPhoneNumber());
    }

    @Override
    public void onClick(View view) {
        String url;
        switch (view.getId()) {
            case R.id.btnSubmit:
                if(shippingId != 0)
                {
                    SharedPreferences.Editor cartEditor = cartPrefs.edit();
                    cartEditor.putString(Variable.FIRST_NAME, billAddress.getRealName());
                    cartEditor.putString(Variable.LAST_NAME, billAddress.getRealName());
                    cartEditor.putString(Variable.ADDRESS, billAddress.getStreetAddress());
                    cartEditor.putString(Variable.STATE, billAddress.getStateName());
                    cartEditor.putInt(Variable.STATE_ID, billAddress.getStateId());
                    cartEditor.putString(Variable.CITY, billAddress.getCityName());
                    cartEditor.putInt(Variable.CITY_ID, billAddress.getCityId());
                    cartEditor.putString(Variable.DISTRICT, billAddress.getDistrictName());
                    cartEditor.putInt(Variable.DISTRICT_ID, billAddress.getDistrictId());
                    cartEditor.putString(Variable.ZIP, billAddress.getPostCode());
                    cartEditor.putString(Variable.PHONE, billAddress.getPhoneNumber());
                    cartEditor.putInt("shipping_id", shippingId);
                    cartEditor.putString("shipping_cost", String.valueOf(shipCost));
                    cartEditor.putString("subtotal", String.valueOf(subTotal));
                    cartEditor.putString("total", String.valueOf(total));
                    cartEditor.commit();

                    Intent payment = new Intent(this, CheckoutPayment.class);
                    startActivity(payment);
                }
                else
                {
                    Snackbar.make(container, "Pilih layanan pengiriman terlebih dulu!", Snackbar.LENGTH_LONG)
                            .show();
                }
                break;

            case R.id.jneReg:
                shippingId = 6;
                url = shippingCostUrl(weight, shippingId);
                new ShippingCostAsyncTask().execute(url);
                break;

            case R.id.jneYes:
                shippingId = 7;
                url = shippingCostUrl(weight, shippingId);
                new ShippingCostAsyncTask().execute(url);
                break;
        }
    }

    private class ShippingCostAsyncTask extends AsyncTask<String, Void, JSONObject> {
        ProgressDialog dialog;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            dialog = new ProgressDialog(CheckoutShipping.this);
            dialog.setMessage("Mohon tunggu, menghitung biaya pengiriman!");
            dialog.show();
        }

        @Override
        protected JSONObject doInBackground(String... params) {
            try {
                JSONObject json = Requests.getData(params[0]);
                System.out.println(json.toString());
                return json;
            } catch (Exception e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(JSONObject json) {
            dialog.dismiss();

            if(json != null) {
                try
                {
                    JSONObject shipping = json.getJSONObject("shipping");

                    shipCost = shipping.getDouble("cost");
                    sumTotal(subTotal, shipCost);
                    updateCartSummary();
                }
                catch (JSONException je)
                {
                    je.printStackTrace();
                }
            }
            else {
                Snackbar.make(container, "Failed to connect server!", Snackbar.LENGTH_SHORT)
                        .show();
            }
        }
    }

    private double sumWeight() {
        weight = 0;

        Carts carts = new Carts(this);
        carts.open();
        listCart    = carts.allCarts();

        if(listCart != null) {
            for (Cart cart : listCart) {
                weight = weight + (cart.getWeight()*cart.getAmount());
            }

            System.out.println(weight);
        }

        carts.close();
        return weight;
    }

    private double sumSubTotal() {
        subTotal = 0;

        Carts temps = new Carts(this);
        temps.open();
        listCart    = temps.allCarts();

        if(listCart != null)
        {
            for (Cart cart : listCart) {
                subTotal = subTotal + cart.getSubtotal();
            }

            System.out.println(subTotal);
        }

        temps.close();
        return subTotal;
    }

    private double sumTotal(double subTotal, double shipCost) {
        total = 0;
        total = subTotal + shipCost;
        return total;
    }

    private void updateCartSummary() {
        //Update text view value
        txtSubTotal.setText(Formatter.accounting(String.valueOf(subTotal), "Rp"));
        txtShipCost.setText(Formatter.accounting(String.valueOf(shipCost), "Rp"));
        txtTotal.setText(Formatter.accounting(String.valueOf(total), "Rp"));
    }

    private String shippingCostUrl(double weight, int shippingId) {
        String url = "";

        url = ApiServices.SHIPPING_COST + "?weight=" + weight + "&shipping_id=" + shippingId +
                "&city=" + billAddress.getDistrictId();

        return url;
    }

}
