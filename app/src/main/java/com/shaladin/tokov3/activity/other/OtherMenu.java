package com.shaladin.tokov3.activity.other;

import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.shaladin.tokov3.R;
import com.shaladin.tokov3.adapter.OtherMenuAdapter;
import com.shaladin.tokov3.utils.DividerItemDecoration;

public class OtherMenu extends AppCompatActivity {

    private String[] menus = {
            "Cara Berbelanja",
            "Aturan Penggunaan",
            "Kebijakan Privasi",
            "Konfirmasi Pembayaran",
            "Hubungi Kami"
    };

    private OtherMenuAdapter otherMenuAdapter;

    RecyclerView otherRecycle;
    View container;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_other);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        initView();
    }

    private void initView() {
        container    = findViewById(R.id.container);
        otherRecycle = (RecyclerView) findViewById(R.id.otherRecycle);

        otherMenuAdapter = new OtherMenuAdapter(this, menus);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        otherRecycle.setLayoutManager(layoutManager);
        otherRecycle.setHasFixedSize(true);
        otherRecycle.setAdapter(otherMenuAdapter);
        otherMenuAdapter.notifyDataSetChanged();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

}
