package com.shaladin.tokov3.activity.products;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.shaladin.tokov3.R;
import com.shaladin.tokov3.activity.CartActivity;
import com.shaladin.tokov3.config.Variable;
import com.shaladin.tokov3.database.Carts;
import com.shaladin.tokov3.utils.Formatter;
import com.shaladin.tokov3.utils.Misc;
import com.shaladin.tokov3.utils.ProductsKeys;
import com.squareup.picasso.Picasso;

import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;

public class ProductDetail extends AppCompatActivity {

    //private properties
    private String product_id;
    private String imageUrl;
    TextView description;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_detail);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        final Intent ProductIntent = getIntent();
        final int amountProduct = Integer.parseInt(ProductIntent.getStringExtra(Variable.AMOUNT));
        final int stepProduct   = Integer.parseInt(ProductIntent.getStringExtra(ProductsKeys.KEY_PRODUCT_STEP));
        product_id  = ProductIntent.getStringExtra(Variable.PRODUCT_ID);
        imageUrl    = ProductIntent.getStringExtra(Variable.IMAGE);
        //set Title
        getSupportActionBar().setTitle(ProductIntent.getStringExtra(Variable.PRODUCT));

        ImageView productImage= (ImageView) findViewById(R.id.image);
        Picasso.with(this).load(ProductIntent.getStringExtra(Variable.IMAGE)).into(productImage);

        TextView productName  = (TextView) findViewById(R.id.productName);
        productName.setText(ProductIntent.getStringExtra(Variable.PRODUCT));

        TextView productPrice = (TextView) findViewById(R.id.productPrice);
        productPrice.setText(Formatter.accounting(ProductIntent.getStringExtra(Variable.PRICE), "Rp"));

        description = (TextView) findViewById(R.id.description);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        Button btnBuy = (Button) findViewById(R.id.btnBuy);
        if(amountProduct == 0) {
            btnBuy.setText("Sold Out");
            btnBuy.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ProductDetail.super.onBackPressed();
                }
            });
        } else {
            btnBuy.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(@Nullable final View view) {
//                Snackbar.make(view, "Hello World ^_^", Snackbar.LENGTH_LONG).show();
                    final CharSequence[] Qty = new CharSequence[Math.round(amountProduct/stepProduct)]; int n = 0;
                    for(int i = 0; i < Qty.length; i++){
                        n = n+stepProduct;
                        Qty[i] = String.valueOf(n);
                    }
                    AlertDialog.Builder qtyPicker = new AlertDialog.Builder(view.getContext());
                    qtyPicker.setTitle("Pilih Quantity Pembelian");
                    qtyPicker.setItems(Qty, new DialogInterface.OnClickListener(){
                        public void onClick(DialogInterface dialog, int j) {
                            Carts carts = new Carts(view.getContext());
                            carts.open();
                            //Toast.makeText(getApplicationContext(), intent.getStringExtra("amount"), Toast.LENGTH_SHORT).show();

                            //product parameters
                            String _productId = ProductIntent.getStringExtra(Variable.PRODUCT_ID);
                            String _product = ProductIntent.getStringExtra(Variable.PRODUCT);
                            String _image   = ProductIntent.getStringExtra(Variable.IMAGE);
                            String _price   = ProductIntent.getStringExtra(Variable.PRICE);
                            String _weight  = ProductIntent.getStringExtra(ProductsKeys.KEY_PRODUCT_WEIGHT);
                            String _amount  = String.valueOf(Qty[j]);

                            //Convert data for required resource
                            int productId   = Integer.parseInt(_productId);
                            int amount      = Integer.parseInt(_amount);
                            double price    = Double.parseDouble(_price);
                            double weight   = Double.parseDouble(_weight);
                            double subtotal = price * amount;

                            //save data to carts
                            carts.createCart(productId,_product,_image,price,amount,weight,subtotal);

                            Context context = view.getContext();
                            Intent troli    = new Intent(context, CartActivity.class);
                            troli.putExtra(Variable.PRODUCT, _product);
                            troli.putExtra(Variable.PRICE, _price);
                            troli.putExtra(Variable.IMAGE, _image);
                            troli.putExtra(Variable.AMOUNT, _amount);
                            context.startActivity(troli);
                        }
                    });
                    AlertDialog picker = qtyPicker.create();
                    picker.show();
                }
            });
        }

        Button saveImage = (Button) findViewById(R.id.saveImage);
        saveImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (checkWriteFilePermission()) {
                    (new Misc()).downloadImage(view.getContext(), imageUrl);
                }
            }
        });
    }

    private boolean checkWriteFilePermission() {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            return true;
        }
        if(ActivityCompat.checkSelfPermission(this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED)
        {
            return true;
        }
        if(shouldShowRequestPermissionRationale(WRITE_EXTERNAL_STORAGE)) {
            requestPermissions(new String[]{WRITE_EXTERNAL_STORAGE}, 0);
        }
        else {
            requestPermissions(new String[]{WRITE_EXTERNAL_STORAGE}, 0);
        }
        return false;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permission[], int[] grantResult) {
        if(requestCode == 0) {
            if(grantResult.length > 0 && grantResult[0] == PackageManager.PERMISSION_GRANTED) {
                (new Misc()).downloadImage(this, imageUrl);
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        //Get Menu ID
        int id = menuItem.getItemId();

        switch (id) {
            case android.R.id.home:
                super.onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(menuItem);
    }
}
