package com.shaladin.tokov3.activity.profile;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.shaladin.tokov3.R;
import com.shaladin.tokov3.activity.profile.address.AddressNew;
import com.shaladin.tokov3.adapter.AddressAdapter;
import com.shaladin.tokov3.config.Variable;
import com.shaladin.tokov3.database.Alamat;
import com.shaladin.tokov3.model.Address;
import com.shaladin.tokov3.utils.DividerItemDecoration;

import java.util.List;

public class ProfileAddress extends AppCompatActivity {

    private AddressAdapter addressAdapter;
    private List<Address> addresses;
    private Alamat alamat;
    private int userId;

    SharedPreferences userPrefs;

    RecyclerView addressRecycle;
    View container;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_address);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        userPrefs   = getSharedPreferences(Variable.USER_PREFERENCE, Context.MODE_PRIVATE);
        userId      = userPrefs.getInt(Variable.USER_ID, 0);

        alamat = new Alamat(this);
        alamat.open();
        addresses   = alamat.getByUserId(userId);
        alamat.close();

        initView();
    }

    @Override
    protected void onResume() {
        super.onResume();

        alamat = new Alamat(this);
        alamat.open();
        addresses   = alamat.getByUserId(userId);
        alamat.close();

        initView();
    }

    private void initView() {
        container = findViewById(R.id.container);

        addressAdapter = new AddressAdapter(this, addresses);
        addressRecycle = (RecyclerView) findViewById(R.id.addressRecycle);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        addressRecycle.setLayoutManager(layoutManager);
        addressRecycle.setHasFixedSize(true);
        addressRecycle.setAdapter(addressAdapter);

        RecyclerView.ItemDecoration itemDecoration = new DividerItemDecoration(this, DividerItemDecoration.VERTICAL_LIST);
        addressRecycle.addItemDecoration(itemDecoration);
        addressAdapter.notifyDataSetChanged();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //inflate the menus
        getMenuInflater().inflate(R.menu.address, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                break;
            case R.id.actionAdd:
                Intent newAddress = new Intent(this, AddressNew.class);
                newAddress.putExtra("action", "create");
                startActivity(newAddress);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

}
