package com.shaladin.tokov3.activity.profile;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.method.PasswordTransformationMethod;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.shaladin.tokov3.R;
import com.shaladin.tokov3.config.Variable;
import com.shaladin.tokov3.utils.Misc;

public class ProfileEmail extends AppCompatActivity implements View.OnClickListener {

    private String currentEmail;
    private String newEmail;
    private String currentPassword;
    private String password;

    SharedPreferences userPrefs;
    EditText iCurrentEmail;
    EditText iNewEmail;
    EditText iPassword;
    Button btnSave;
    View container;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_email);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        //get user credentials
        userPrefs       = getSharedPreferences(Variable.USER_PREFERENCE, Context.MODE_PRIVATE);
        currentPassword = userPrefs.getString(Variable.PASSWORD, null);

        initView();
    }

    private void initView() {
        container = findViewById(R.id.container);

        //initial view
        iCurrentEmail   = (EditText) findViewById(R.id.inputCurrentEmail);
        iNewEmail       = (EditText) findViewById(R.id.inputNewEmail);
        iPassword       = (EditText) findViewById(R.id.inputCurrentPassword);

        iPassword.setTransformationMethod(new PasswordTransformationMethod());
        iCurrentEmail.setFocusable(false);

        if(userPrefs.contains(Variable.EMAIL))
        {
            iCurrentEmail.setText(userPrefs.getString(Variable.EMAIL, null));
        }

        btnSave = (Button) findViewById(R.id.btnSave);
        btnSave.setOnClickListener(this);
    }

    private boolean validation() {
        boolean isValid = true;

        newEmail = String.valueOf(iNewEmail.getText());
        password = Misc.encodeBase64(String.valueOf(iPassword.getText()));

        if(newEmail.isEmpty())
        {
            iNewEmail.setError(getString(R.string.new_email) + " " + getString(R.string.input_required));
            iNewEmail.requestFocus();
            isValid = false;
        }
        else if(!Misc.isValidEmail(newEmail))
        {
            iNewEmail.setError("Invalid " + getString(R.string.new_email));
            iNewEmail.requestFocus();
            isValid = false;
        }
        else if(password.isEmpty())
        {
            iPassword.setError(getString(R.string.input_password) + " " + getString(R.string.input_required));
            iPassword.requestFocus();
            isValid = false;
        }
        else  if(!password.equals(currentPassword))
        {
            iPassword.setError(getString(R.string.input_password) + " " + getString(R.string.message_not_match));
            iPassword.requestFocus();
            isValid = false;
        }

        return isValid;
    }

    private void doSaveData() {
        //Initial SharedPreference Editor
        SharedPreferences.Editor editor = userPrefs.edit();
        editor.putString(Variable.EMAIL, newEmail);
        editor.commit();

        //reset view
        iCurrentEmail.setText(newEmail);
        iNewEmail.setText(null);
        iPassword.setText(null);

        Snackbar.make(container, getString(R.string.new_email) + " berhasil di simpan!", Snackbar.LENGTH_LONG)
                .show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View view) {

        switch (view.getId())
        {
            case R.id.btnSave:
                if(validation()) doSaveData();
                break;
        }
    }
}
