package com.shaladin.tokov3.activity.profile;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.shaladin.tokov3.R;
import com.shaladin.tokov3.config.Variable;

import java.util.Calendar;

public class ProfileInformation extends AppCompatActivity implements View.OnClickListener {

    private String firstname;
    private String lastname;
    private String birthdate;
    private String gender;

    SharedPreferences UserPrefs;
    EditText inputFirstName;
    EditText inputLastName;
    EditText inputBirthDate;
    RadioGroup radioGender;
    Button btnSave;
    View container;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_information);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        UserPrefs = getSharedPreferences(Variable.USER_PREFERENCE, Context.MODE_PRIVATE);
        initView();
    }

    private void initView() {
        container = findViewById(R.id.container);
        //initial view
        inputFirstName  = (EditText) findViewById(R.id.inputFirstname);
        inputLastName   = (EditText) findViewById(R.id.inputLastname);
        inputBirthDate  = (EditText) findViewById(R.id.inputDateBirth);
        radioGender     = (RadioGroup) findViewById(R.id.radioGender);

        RadioButton genderMale  = (RadioButton) findViewById(R.id.genderMale);
        RadioButton genderFemale= (RadioButton) findViewById(R.id.genderFemale);

        inputBirthDate.setFocusable(false);
        inputBirthDate.setOnClickListener(this);

        if(UserPrefs.contains(Variable.FIRST_NAME))
            inputFirstName.setText(UserPrefs.getString(Variable.FIRST_NAME, null));

        if(UserPrefs.contains(Variable.LAST_NAME))
            inputLastName.setText(UserPrefs.getString(Variable.LAST_NAME, null));

        if(UserPrefs.contains(Variable.DATE_BIRTH))
            inputBirthDate.setText(UserPrefs.getString(Variable.DATE_BIRTH, null));

        if(UserPrefs.contains(Variable.GENDER))
        {
            switch (UserPrefs.getString(Variable.GENDER, null)) {
                case "Laki-laki":
                    genderMale.setChecked(true);
                    break;

                case "Perempuan":
                    genderFemale.setChecked(true);
                    break;
            }
        }

        btnSave         = (Button) findViewById(R.id.btnSave);
        btnSave.setOnClickListener(this);
    }

    private boolean validator() {
        boolean isValid = true;
        String message;
        int selectedGender = radioGender.getCheckedRadioButtonId();
        //get value from view
        firstname       = String.valueOf(inputFirstName.getText());
        lastname        = String.valueOf(inputLastName.getText());
        birthdate       = String.valueOf(inputBirthDate.getText());

        if(firstname.isEmpty())
        {
            message = getString(R.string.input_firstname) +" "+ getString(R.string.input_required);
            inputFirstName.setError(message);
            inputFirstName.requestFocus();
            isValid = false;
        }
        else if (lastname.isEmpty())
        {
            message = getString(R.string.input_lastname) +" "+ getString(R.string.input_required);
            inputLastName.setError(message);
            inputLastName.requestFocus();
            isValid = false;
        }
        else if (birthdate.isEmpty())
        {
            message = getString(R.string.input_birth_date) +" "+ getString(R.string.input_required);
            inputBirthDate.setError(message);
            inputBirthDate.requestFocus();
            isValid = false;
        }

        else if(selectedGender == -1)
        {
            isValid = false;
            Snackbar.make(container, getString(R.string.message_gender_required), Snackbar.LENGTH_LONG)
                    .show();
        }
        else
        {
            switch (selectedGender) {
                case R.id.genderMale:
                    gender = getString(R.string.gender_male);
                    break;

                case R.id.genderFemale:
                    gender = getString(R.string.gender_female);
                    break;
            }
        }

        return isValid;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    private void showDatePicker(View view) {
        DialogFragment dialog = new DatePickerFragment();
        dialog.show(getSupportFragmentManager(), "datePicker");
    }

    public static class DatePickerFragment extends DialogFragment implements DatePickerDialog.OnDateSetListener {

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            Calendar c  = Calendar.getInstance();
            int year    = c.get(Calendar.YEAR);
            int month   = c.get(Calendar.MONTH);
            int day     = c.get(Calendar.DAY_OF_MONTH);

            //Create new instance of DatePickerDialgo
            return new DatePickerDialog(getContext(), this, year, month, day);
        }

        public void onDateSet(DatePicker view, int year, int month, int day) {
            //Set selected date to view
            setBirthDate(year, month+1, day);
        }

        private void setBirthDate(int year, int month, int day) {
            EditText birthDate = (EditText) getActivity().findViewById(R.id.inputDateBirth);
            StringBuilder d    = new StringBuilder().append(day).append("/").append(String.format("%02d", month))
                    .append("/").append(year);

            birthDate.setText(d);
        }
    }

    private void saveData() {
        //Shared Preferences Editor
        SharedPreferences.Editor editor = UserPrefs.edit();

        if(!UserPrefs.getString(Variable.FIRST_NAME,null).equals(firstname)) {
            editor.putString(Variable.FIRST_NAME, firstname);
        }

        if(!UserPrefs.getString(Variable.LAST_NAME,null).equals(lastname)) {
            editor.putString(Variable.LAST_NAME, lastname);
        }

        if(!UserPrefs.contains(Variable.DATE_BIRTH))
        {
            editor.putString(Variable.DATE_BIRTH, birthdate);
        }
        else if (!UserPrefs.getString(Variable.DATE_BIRTH, null).equals(birthdate))
        {
            editor.putString(Variable.DATE_BIRTH, birthdate);
        }

        if(!UserPrefs.contains(Variable.GENDER))
        {
            editor.putString(Variable.GENDER, gender);
        }
        else if(!UserPrefs.getString(Variable.GENDER, null).equals(gender))
        {
            editor.putString(Variable.GENDER, gender);
        }

        editor.commit();
        String message = getString(R.string.title_activity_personal_information) + " " +
                getString(R.string.message_data_saved);

        Snackbar.make(container, message, Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnSave:
                if(validator()) saveData();
                break;

            case R.id.inputDateBirth:
                showDatePicker(view);
                break;
        }
    }
}
