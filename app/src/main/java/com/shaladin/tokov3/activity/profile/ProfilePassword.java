package com.shaladin.tokov3.activity.profile;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.shaladin.tokov3.R;
import com.shaladin.tokov3.config.Variable;
import com.shaladin.tokov3.utils.Misc;

public class ProfilePassword extends AppCompatActivity implements View.OnClickListener {

    private String password;
    private String currentPassword;
    private String newPassword;
    private String retypePassword;

    SharedPreferences userPrefs;
    EditText iCurrentPassword;
    EditText iNewPassword;
    EditText iRetypePassword;
    Button btnSave;
    View container;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_password);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        //get user credentials
        userPrefs = getSharedPreferences(Variable.USER_PREFERENCE, Context.MODE_PRIVATE);
        password  = userPrefs.getString(Variable.PASSWORD, null);

        initView();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    private void initView() {

        container = findViewById(R.id.container);
        //prepare rendering view
        iCurrentPassword = (EditText) findViewById(R.id.inputCurrentPassword);
        iNewPassword     = (EditText) findViewById(R.id.inputNewPassword);
        iRetypePassword  = (EditText) findViewById(R.id.inputRetypePassword);

        btnSave = (Button) findViewById(R.id.btnSave);
        btnSave.setOnClickListener(this);
    }

    private boolean validation() {
        boolean isValid = true;

        currentPassword = Misc.encodeBase64(String.valueOf(iCurrentPassword.getText()));
        newPassword     = Misc.encodeBase64(String.valueOf(iNewPassword.getText()));
        retypePassword  = Misc.encodeBase64(String.valueOf(iRetypePassword.getText()));

        if(currentPassword.isEmpty()) {
            iCurrentPassword.setError(getString(R.string.current_password) + " " + getString(R.string.input_required));
            iCurrentPassword.requestFocus();
            isValid = false;
        }
        else if(newPassword.isEmpty()) {
            iNewPassword.setError(getString(R.string.new_password) + " " + getString(R.string.input_required));
            iNewPassword.requestFocus();
            isValid = false;
        }
        else if(retypePassword.isEmpty()) {
            iRetypePassword.setError(getString(R.string.new_password) + " " + getString(R.string.input_required));
            iRetypePassword.requestFocus();
            isValid = false;
        }
        else if(!newPassword.equals(retypePassword)) {
            iNewPassword.setError(getString(R.string.new_password) + " " + getString(R.string.message_not_match));
            iNewPassword.requestFocus();
            isValid = false;
        }
        else if(!password.equals(currentPassword)) {
            iCurrentPassword.setError(getString(R.string.current_password) + " " + getString(R.string.message_not_match));
            iCurrentPassword.requestFocus();
            isValid = false;
        }


        return isValid;
    }

    private void changePassword() {
        SharedPreferences.Editor editor = userPrefs.edit();
        editor.putString(Variable.PASSWORD, newPassword);
        editor.commit();

        iCurrentPassword.setText(null);
        iNewPassword.setText(null);
        iRetypePassword.setText(null);

        Snackbar.make(container, getString(R.string.input_password) + " berhasil di update!", Snackbar.LENGTH_LONG)
                .show();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnSave:
                if(validation()) changePassword();
                break;
        }
    }
}
