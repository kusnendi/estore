package com.shaladin.tokov3.activity.profile;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.method.PasswordTransformationMethod;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.shaladin.tokov3.R;
import com.shaladin.tokov3.config.Variable;
import com.shaladin.tokov3.utils.Misc;

public class ProfilePhone extends AppCompatActivity implements View.OnClickListener {

    private String password;
    private String currentPhone;
    private String newPhone;

    SharedPreferences userPrefs;
    View container;
    EditText iPassword;
    EditText iCurrentPhone;
    EditText iNewPhone;
    Button bSave;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_phone);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        userPrefs = getSharedPreferences(Variable.USER_PREFERENCE, Context.MODE_PRIVATE);
        initView();
    }

    private void initView() {
        //Initial view
        container       = findViewById(R.id.container);
        iPassword       = (EditText) findViewById(R.id.inputCurrentPassword);
        iCurrentPhone   = (EditText) findViewById(R.id.inputCurrentPhone);
        iNewPhone       = (EditText) findViewById(R.id.inputNewPhone);

        iPassword.setTransformationMethod(new PasswordTransformationMethod());
        iCurrentPhone.setFocusable(false);

        //Set current value
        if(userPrefs.contains(Variable.PHONE))
        {
            iCurrentPhone.setText(userPrefs.getString(Variable.PHONE, null));
        }

        bSave = (Button) findViewById(R.id.btnSave);
        bSave.setOnClickListener(this);
    }

    private boolean validation() {
        //get value from view
        password = Misc.encodeBase64(String.valueOf(iPassword.getText()));
        currentPhone = String.valueOf(iCurrentPhone.getText());
        newPhone = String.valueOf(iNewPhone.getText());

        boolean isValid = true;
        if(newPhone.isEmpty())
        {
            iNewPhone.setError(getString(R.string.new_phone) + " " + getString(R.string.input_required));
            iNewPhone.requestFocus();
            isValid = false;
        }
        else if(password.isEmpty())
        {
            iPassword.setError(getString(R.string.input_password) + " " + getString(R.string.input_required));
            iPassword.requestFocus();
            isValid = false;
        }
        else if(password.length() > 0 && !password.equals(userPrefs.getString(Variable.PASSWORD, null)))
        {
            iPassword.setError(getString(R.string.input_password) + " " + getString(R.string.message_not_match));
            iPassword.requestFocus();
            isValid = false;
        }

        return isValid;
    }

    private void doChangePhone() {
        SharedPreferences.Editor editor = userPrefs.edit();
        editor.putString(Variable.PHONE, newPhone);
        editor.commit();

        //reset view
        iCurrentPhone.setText(newPhone);
        iNewPhone.setText(null);
        iPassword.setText(null);

        Snackbar.make(container, getString(R.string.new_phone) + " berhasil di simpan!", Snackbar.LENGTH_LONG)
                .show();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnSave:
                if(validation()) doChangePhone();
                break;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                break;
        }

        return super.onOptionsItemSelected(item);
    }
}
