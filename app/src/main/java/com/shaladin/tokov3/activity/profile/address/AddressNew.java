package com.shaladin.tokov3.activity.profile.address;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.shaladin.tokov3.R;
import com.shaladin.tokov3.config.ApiServices;
import com.shaladin.tokov3.config.Variable;
import com.shaladin.tokov3.database.Alamat;
import com.shaladin.tokov3.http.Requests;
import com.shaladin.tokov3.model.Address;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class AddressNew extends AppCompatActivity implements AdapterView.OnItemSelectedListener {

    private HashMap<String, Integer> mapStates = new HashMap<>();
    private HashMap<String, Integer> mapCities = new HashMap<>();
    private HashMap<String, Integer> mapDistricts = new HashMap<>();
    private ArrayList<String> states = new ArrayList<>();
    private ArrayList<String> cities = new ArrayList<>();
    private ArrayList<String> districts = new ArrayList<>();
    private ArrayAdapter<String> statesAdapter;
    private ArrayAdapter<String> citiesAdapter;
    private ArrayAdapter<String> districtsAdapter;
    private SharedPreferences userPrefs;
    private SharedPreferences statesPrefs;
    private Address address;
    private String addressName;
    private String asName;
    private String phoneNumber;
    private String stateName;
    private String cityName;
    private String districtName;
    private String streetAddress;
    private String postCode;
    private String action;
    private int userId;
    private int stateId;
    private int cityId;
    private int districtId;
    private long id;
    private Intent aIntent;
    EditText iAddressName;
    EditText iAsName;
    EditText iPhone;
    Spinner iState;
    Spinner iCity;
    Spinner iDistrict;
    EditText iAddress;
    EditText iPostCode;
    View container;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_address_new);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        statesPrefs     = getSharedPreferences(Variable.STATES_PREFERENCE, Context.MODE_PRIVATE);
        userPrefs       = getSharedPreferences(Variable.USER_PREFERENCE, Context.MODE_PRIVATE);
        userId          = userPrefs.getInt(Variable.USER_ID, 0);

        aIntent = getIntent();
        action  = aIntent.getStringExtra("action");
        initView();
    }

    private void initView() {

        container    = findViewById(R.id.container);
        //Initial view for rendering
        iAddressName = (EditText) findViewById(R.id.inputAddressName);
        iAsName      = (EditText) findViewById(R.id.inputAsName);
        iPhone       = (EditText) findViewById(R.id.inputTelepon);
        iState       = (Spinner) findViewById(R.id.inputState);
        iCity        = (Spinner) findViewById(R.id.inputCity);
        iDistrict    = (Spinner) findViewById(R.id.inputDistrict);
        iAddress     = (EditText) findViewById(R.id.inputAddress);
        iPostCode    = (EditText) findViewById(R.id.inputPostCode);

        if(aIntent.getStringExtra("action").equals("update"))
        {
            Bundle data = aIntent.getBundleExtra("data");
            address     = (Address) data.getSerializable("address");

            iAddressName.setText(address.getName());
            iAsName.setText(address.getRealName());
            iPhone.setText(address.getPhoneNumber());
            iAddress.setText(address.getStreetAddress());
            iPostCode.setText(address.getPostCode());

            id            = address.getId();
            addressName   = address.getName();
            asName        = address.getRealName();
            phoneNumber   = address.getPhoneNumber();
            stateId       = address.getStateId();
            stateName     = address.getStateName();
            cityId        = address.getCityId();
            cityName      = address.getCityName();
            districtId    = address.getDistrictId();
            districtName  = address.getDistrictName();
            streetAddress = address.getStreetAddress();
            postCode      = address.getPostCode();
        }

        states.add(getString(R.string.option_choose_state));
        if(statesPrefs.contains(Variable.STATE)) initStates(statesPrefs.getString(Variable.STATE, null));
        statesAdapter= new ArrayAdapter<String>(this, R.layout.item_spinner, states) {
            @Override
            public boolean isEnabled(int position) {
                if(position == 0) return false;
                else return true;
            }

            @Override
            public View getDropDownView(int position, View convertView, ViewGroup parent) {

                View view = super.getDropDownView(position, convertView, parent);
                TextView textView = (TextView) view;
                if(position == 0)
                {
                    //Set hint text color
                    textView.setTextColor(Color.GRAY);
                }
                else
                {
                    textView.setTextColor(Color.BLACK);
                }

                return view;
            }
        };
        statesAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        iState.setAdapter(statesAdapter);
        iState.setOnItemSelectedListener(this);
        if(stateId > 0) iState.setSelection(statesAdapter.getPosition(address.getStateName()));
        else iState.setSelection(0);

        cities.add(getString(R.string.option_choose_city));
        citiesAdapter = new ArrayAdapter<String>(this, R.layout.item_spinner, cities) {
            @Override
            public boolean isEnabled(int position) {
                if(position == 0) return false;
                else return true;
            }

            @Override
            public View getDropDownView(int position, View convertView, ViewGroup parent) {

                View view = super.getDropDownView(position, convertView, parent);
                TextView textView = (TextView) view;
                if(position == 0)
                {
                    //Set hint text color
                    textView.setTextColor(Color.GRAY);
                }
                else
                {
                    textView.setTextColor(Color.BLACK);
                }

                return view;
            }
        };
        citiesAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        iCity.setAdapter(citiesAdapter);
        iCity.setOnItemSelectedListener(this);
        iCity.setEnabled(false);

        districts.add(getString(R.string.option_choose_district));
        districtsAdapter = new ArrayAdapter<String>(this, R.layout.item_spinner, districts) {
            @Override
            public boolean isEnabled(int position) {
                if(position == 0) return false;
                else return true;
            }
            @Override
            public View getDropDownView(int position, View convertView, ViewGroup parent) {
                View view = super.getDropDownView(position, convertView, parent);
                TextView textView = (TextView) view;
                if(position == 0)
                {
                    textView.setTextColor(Color.GRAY);
                }
                else
                {
                    textView.setTextColor(Color.BLACK);
                }

                return view;
            }
        };
        districtsAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        iDistrict.setAdapter(districtsAdapter);
        iDistrict.setOnItemSelectedListener(this);
        iDistrict.setEnabled(false);
    }

    private boolean validation() {
        boolean isValid = true;

        addressName = String.valueOf(iAddressName.getText());
        asName      = String.valueOf(iAsName.getText());
        phoneNumber = String.valueOf(iPhone.getText());
        streetAddress = String.valueOf(iAddress.getText());
        postCode    = String.valueOf(iPostCode.getText());

        if(addressName.isEmpty())
        {
            iAddressName.setError(getString(R.string.input_address_name) + " " + getString(R.string.input_required));
            iAddressName.requestFocus();
            isValid = false;
        }
        else if(asName.isEmpty())
        {
            iAsName.setError(getString(R.string.input_as_name) + " " + getString(R.string.input_required));
            iAsName.requestFocus();
            isValid = false;
        }
        else if(phoneNumber.isEmpty())
        {
            iPhone.setError(getString(R.string.input_telepon) + " " + getString(R.string.input_required));
            iPhone.requestFocus();
            isValid = false;
        }
        else if(stateId == 0)
        {
            Snackbar.make(container, getString(R.string.input_state) + " " + getString(R.string.input_required),
                    Snackbar.LENGTH_LONG).show();
            isValid = false;
        }
        else if(cityId == 0)
        {
            Snackbar.make(container, getString(R.string.input_city) + " " + getString(R.string.input_city),
                    Snackbar.LENGTH_LONG).show();
            isValid = false;
        }
        else if(districtId == 0)
        {
            Snackbar.make(container, getString(R.string.input_district) + " " + getString(R.string.input_district),
                    Snackbar.LENGTH_LONG).show();
            isValid = false;
        }
        else if(streetAddress.isEmpty())
        {
            iAddress.setError(getString(R.string.input_address) + " " + getString(R.string.input_required));
            iAddress.requestFocus();
            isValid = false;
        }
        else if(postCode.isEmpty())
        {
            iPostCode.setError(getString(R.string.input_post_code) + " " + getString(R.string.input_required));
            iPostCode.requestFocus();
            isValid = false;
        }

        return isValid;
    }

    private int initStates(String stringJson) {
        int len = 0;
        try
        {
            JSONArray statesArray = new JSONArray(stringJson);
            len = statesArray.length();

            if(len > 0)
            {
                for (int i = 0; i < len; i++)
                {
                    JSONObject state = statesArray.getJSONObject(i);
                    String stateName = state.getString(Variable.STATE);
                    int stateId      = state.getInt(Variable.STATE_ID);

                    states.add(stateName);
                    mapStates.put(stateName, stateId);
                }
            }
        }
        catch (JSONException je)
        {
            je.printStackTrace();
        }

        return len;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.address_new, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                break;

            case R.id.btnSave:
                if(validation())
                {
                    long id;
                    switch (action) {
                        case "update":
                            id = updateData();
                            if (id > 0) {
                                super.onBackPressed();
                            }
                            break;

                        case "create":
                            id = saveData();
                            if (id > 0) {
                                super.onBackPressed();
                            }
                            break;
                    }
                }
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    private long updateData() {

        Alamat alamat = new Alamat(this);
        alamat.open();

        address = new Address();
        address.setName(addressName);
        address.setRealName(asName);
        address.setPhoneNumber(phoneNumber);
        address.setStateId(stateId);
        address.setStateName(stateName);
        address.setCityId(cityId);
        address.setCityName(cityName);
        address.setDistrictId(districtId);
        address.setDistrictName(districtName);
        address.setStreetAddress(streetAddress);
        address.setPostCode(postCode);

        id = alamat.update(address, id);
        return id;
    }

    private long saveData() {
        Alamat alamat = new Alamat(this);
        alamat.open();

        address = new Address();
        address.setPrimary(0);
        address.setName(addressName);
        address.setUserId(userId);
        address.setRealName(asName);
        address.setPhoneNumber(phoneNumber);
        address.setStateId(stateId);
        address.setStateName(stateName);
        address.setCityId(cityId);
        address.setCityName(cityName);
        address.setDistrictId(districtId);
        address.setDistrictName(districtName);
        address.setStreetAddress(streetAddress);
        address.setPostCode(postCode);

        long id = alamat.create(address);
        return id;
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
        switch (adapterView.getId()) {
            case R.id.inputState:
                stateName = adapterView.getItemAtPosition(position).toString();

                districts.clear();
                districts.add(getString(R.string.option_choose_district));
                iDistrict.setEnabled(false);
                iDistrict.setSelection(0);

                if(position > 0)
                {
                    stateId   = mapStates.get(stateName);
                    new CitiesAsyncTask().execute(ApiServices.KOTA + stateId);
                }
                break;

            case R.id.inputCity:
                cityName = adapterView.getItemAtPosition(position).toString();

                if(position > 0)
                {
                    cityId    = mapCities.get(cityName);
                    new DistrictsAsyncTask().execute(ApiServices.KECAMATAN + cityId);
                }
                break;

            case R.id.inputDistrict:
                districtName = adapterView.getItemAtPosition(position).toString();

                if(position > 0)
                {
                    districtId = mapDistricts.get(districtName);
                }
                break;
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }

    private class CitiesAsyncTask extends AsyncTask<String, Void, JSONObject> {
        @Override
        protected JSONObject doInBackground(String... params) {
            try
            {
                JSONObject jsonObject = Requests.getData(params[0]);
                return jsonObject;
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }

            return null;
        }
        @Override
        protected void onPostExecute(JSONObject json) {
            if(json != null)
            {
                try
                {
                    boolean isError = json.getBoolean(Variable.ERROR);
                    if(!isError) {
                        JSONArray response = json.getJSONArray(Variable.RESPONSE);
                        int len = response.length();

                        if(len > 0) {

                            cities.clear();
                            cities.add(getString(R.string.option_choose_city));

                            for(int i = 0; i < len; i++)
                            {
                                JSONObject city = response.getJSONObject(i);
                                String name     = city.getString("nama_kabupaten");
                                int id          = city.getInt("id");

                                mapCities.put(name, id);
                                cities.add(name);
                            }

                            if(address != null && address.getCityId() > 0) iCity.setSelection(citiesAdapter.getPosition(address.getCityName()));
                            else iCity.setSelection(0);

                            iCity.setEnabled(true);
                            citiesAdapter.notifyDataSetChanged();
                        }
                    }
                }
                catch (JSONException je)
                {
                    je.printStackTrace();
                }
            }
        }
    }

    private class DistrictsAsyncTask extends AsyncTask<String, Void, JSONObject> {
        @Override
        protected JSONObject doInBackground(String... params) {
            try {
                JSONObject json = Requests.getData(params[0]);
                return json;
            } catch (Exception e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(JSONObject json) {
            if(json != null)
            {
                try {
                    boolean isError = json.getBoolean(Variable.ERROR);
                    if(!isError)
                    {
                        JSONArray response = json.getJSONArray(Variable.RESPONSE);
                        int len = response.length();

                        if(len > 0)
                        {
                            districts.clear();
                            districts.add(getString(R.string.option_choose_district));

                            for(int i = 0; i < len; i++)
                            {
                                JSONObject district = response.getJSONObject(i);
                                String name = district.getString("nama_kota");
                                int id      = district.getInt("id");

                                mapDistricts.put(name, id);
                                districts.add(name);
                            }

                            if(address != null && address.getDistrictId() > 0) iDistrict.setSelection(districtsAdapter.getPosition(address.getDistrictName()));
                            else iDistrict.setSelection(0);

                            iDistrict.setEnabled(true);
                            districtsAdapter.notifyDataSetChanged();
                        }
                    }
                } catch (JSONException je) {
                    je.printStackTrace();
                }
            }
        }
    }

}