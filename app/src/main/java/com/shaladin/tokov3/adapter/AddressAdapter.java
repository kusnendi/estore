package com.shaladin.tokov3.adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.shaladin.tokov3.R;
import com.shaladin.tokov3.activity.profile.address.AddressNew;
import com.shaladin.tokov3.database.Alamat;
import com.shaladin.tokov3.model.Address;

import java.util.List;

/**
 * Created by EliteBook on 5/22/2017.
 */

public class AddressAdapter extends RecyclerView.Adapter<AddressAdapter.AddressViewHolder> implements View.OnClickListener {
    private List<Address> addresses;
    private Context context;

    public AddressAdapter(Context context, List<Address> addresses) {
        this.context    = context;
        this.addresses  = addresses;
    }

    @Override
    public AddressViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new AddressViewHolder(LayoutInflater.from(parent.getContext()), parent);
    }

    @Override
    public void onBindViewHolder(AddressViewHolder holder, final int position) {
        final Address address = addresses.get(position);

        if(address.getPrimary() == 0)
        {
            holder.isPrimary.setVisibility(View.GONE);
            holder.btnPrimary.setVisibility(View.VISIBLE);
        }

        holder.addressName.setText(address.getName());
        holder.personName.setText(address.getRealName());
        holder.streetAddress.setText(address.getStreetAddress());
        holder.districtCity.setText(address.getDistrictName() + ", " + address.getCityName().replaceAll("Kota ", ""));
        holder.statePost.setText(address.getStateName() + ", " + address.getPostCode());
        holder.phoneNumber.setText(address.getPhoneNumber());

        holder.btnEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle bundle = new Bundle();
                bundle.putSerializable("address", address);

                Intent editAddress = new Intent(context, AddressNew.class);
                editAddress.putExtra("action", "update");
                editAddress.putExtra("data", bundle);
                context.startActivity(editAddress);

            }
        });

        holder.btnPrimary.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //
            }
        });

        holder.btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addresses.remove(address);
                Alamat alamat = new Alamat(context);
                alamat.open();
                alamat.delete(address.getId());

                Snackbar.make(view, "Alamat " + address.getName() + " berhasil di hapus!", Snackbar.LENGTH_LONG)
                        .show();
                notifyItemRemoved(position);
            }
        });

        holder.itemView.setOnClickListener(this);
    }

    @Override
    public int getItemCount() {
        return (null != addresses ? addresses.size() : 0);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnEdit:

                break;
        }
    }

    public class AddressViewHolder extends RecyclerView.ViewHolder {
        public TextView isPrimary;
        public TextView addressName;
        public TextView personName;
        public TextView streetAddress;
        public TextView districtCity;
        public TextView statePost;
        public TextView phoneNumber;
        public Button btnEdit;
        public Button btnPrimary;
        public Button btnDelete;

        public AddressViewHolder(LayoutInflater inflater, final ViewGroup parent) {

            super(inflater.inflate(R.layout.item_address, parent, false));

            isPrimary       = (TextView) itemView.findViewById(R.id.textMainAddress);
            addressName     = (TextView) itemView.findViewById(R.id.addressName);
            personName      = (TextView) itemView.findViewById(R.id.personName);
            streetAddress   = (TextView) itemView.findViewById(R.id.streetAddress);
            districtCity    = (TextView) itemView.findViewById(R.id.districtCity);
            statePost       = (TextView) itemView.findViewById(R.id.statePost);
            phoneNumber     = (TextView) itemView.findViewById(R.id.phoneNumber);
            btnEdit         = (Button) itemView.findViewById(R.id.btnEdit);
            btnPrimary      = (Button) itemView.findViewById(R.id.btnSetPrimary);
            btnDelete       = (Button) itemView.findViewById(R.id.btnDelete);
        }
    }
}
