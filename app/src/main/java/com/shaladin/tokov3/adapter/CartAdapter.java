package com.shaladin.tokov3.adapter;

import android.content.Context;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.shaladin.tokov3.R;
import com.shaladin.tokov3.database.Carts;
import com.shaladin.tokov3.model.Cart;
import com.squareup.picasso.Picasso;

import java.text.DecimalFormat;
import java.util.List;

/**
 * Created by EliteBook on 8/16/2016.
 */
public class CartAdapter extends RecyclerView.Adapter<CartAdapter.CartViewHolder> {
    private List<Cart> cartList;
    private Context context;
    OnDataChangeListener mOnDataChangeListener;

    public CartAdapter(Context context, List<Cart> cartList) {
        this.context = context;
        this.cartList= cartList;
    }

    @Override
    public CartViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new CartViewHolder(LayoutInflater.from(parent.getContext()), parent);
    }

    @Override
    public void onBindViewHolder(final CartViewHolder holder, final int position) {
        final Cart cart = cartList.get(position);
        holder.productName.setText(cart.getProduct());
        final DecimalFormat formatter = new DecimalFormat("#,###");
        holder.productPrice.setText("Rp. " + formatter.format(cart.getPrice()));
        holder.productSubtotal.setText("Rp. " + formatter.format(cart.getSubtotal()));
        holder.productQty.setText(String.valueOf(cart.getAmount()));
        holder.productQty.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {}

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {}

            @Override
            public void afterTextChanged(Editable s) {
                String newValue = s.toString();

                if(newValue.length() > 0){
                    int qty             = Integer.parseInt(s.toString());
                    double price        = cart.getPrice();
                    double newSubTotal  = price * qty;

                    Carts c = new Carts(context);
                    c.open();
                    c.updateQuantity(cart.getPrice(), qty, cart.getProductId());
                    c.close();

                    //Update Current Subtotal
                    holder.productSubtotal.setText("Rp. " + formatter.format(newSubTotal));
                    DataChanged();
                }
            }
        });
        holder.productNote.setText(cart.getNote());
        holder.productNote.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                //
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                //
            }

            @Override
            public void afterTextChanged(Editable editable) {
                if(editable.length() > 0)
                {
                    String notes = editable.toString();

                    Carts c = new Carts(context);
                    c.open();
                    c.updateNotes(notes, cart.getProductId());
                    c.close();
                }
            }
        });
        Picasso.with(context).load(cart.getImage()).placeholder(R.mipmap.ic_launcher).into(holder.productImage);

        holder.deleteItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cartList.remove(cart);
                Carts carts = new Carts(v.getContext());
                carts.open();
                carts.deleteCart(cart);

                DataChanged();

//                TextView sumSubtotal = (TextView) holder.
                Snackbar.make(v, "Item " + cart.getProduct() + " berhasil di hapus.", Snackbar.LENGTH_LONG).show();
                notifyDataSetChanged();
            }
        });

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //
            }
        });
    }

    @Override
    public int getItemCount() {
        return (null != cartList ? cartList.size() : 0);
    }

    private void DataChanged() {
        if(mOnDataChangeListener != null){
            mOnDataChangeListener.onDataChanged(cartList.size());
        }
    }

    public interface OnDataChangeListener {
        public void onDataChanged(int size);
    }

    public void setOnDataChangeListener(OnDataChangeListener onDataChangeListener) {
        mOnDataChangeListener = onDataChangeListener;
    }

    public class CartViewHolder extends RecyclerView.ViewHolder {
        public ImageView productImage;
        public TextView productName;
        public TextView productPrice;
        public TextView productSubtotal;
        public EditText productQty;
        public EditText productNote;
        public ImageButton deleteItem;

        public CartViewHolder(LayoutInflater inflater, final ViewGroup parent) {
            super(inflater.inflate(R.layout.item_cart, parent, false));

            productImage = (ImageView) itemView.findViewById(R.id.productImage);
            productName  = (TextView) itemView.findViewById(R.id.productName);
            productPrice = (TextView) itemView.findViewById(R.id.productPrice);
            productQty   = (EditText) itemView.findViewById(R.id.productQty);
            productNote  = (EditText) itemView.findViewById(R.id.productNote);
            productSubtotal = (TextView) itemView.findViewById(R.id.productSubTotal);
            deleteItem   = (ImageButton) itemView.findViewById(R.id.deleteItem);
        }
    }
}
