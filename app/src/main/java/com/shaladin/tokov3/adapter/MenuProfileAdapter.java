package com.shaladin.tokov3.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.shaladin.tokov3.R;
import com.shaladin.tokov3.activity.other.OtherMenu;
import com.shaladin.tokov3.activity.profile.ProfileAddress;
import com.shaladin.tokov3.activity.profile.ProfileEmail;
import com.shaladin.tokov3.activity.profile.ProfileInformation;
import com.shaladin.tokov3.activity.profile.ProfilePassword;
import com.shaladin.tokov3.activity.profile.ProfilePhone;
import com.shaladin.tokov3.config.Variable;
import com.shaladin.tokov3.utils.Misc;

/**
 * Created by EliteBook on 5/12/2017.
 */

public class MenuProfileAdapter extends RecyclerView.Adapter<MenuProfileAdapter.MenuProfileViewHolder>{
    private Context context;
    private int[] menuIcons;
    private String[] menuTitles;

    public MenuProfileAdapter(Context context, String[] menuTitles, int[] menuIcons) {
        this.context = context;
        this.menuTitles = menuTitles;
        this.menuIcons  = menuIcons;
    }

    @Override
    public MenuProfileViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new MenuProfileViewHolder(LayoutInflater.from(parent.getContext()), parent);
    }

    @Override
    public void onBindViewHolder(MenuProfileViewHolder holder, final int position) {
        holder.menuTitle.setText(menuTitles[position]);
        holder.menuIcon.setImageResource(menuIcons[position]);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                toMenu(menuTitles[position]);
            }
        });
    }

    @Override
    public int getItemCount() {
        return (null != menuTitles ? menuTitles.length : 0);
    }

    private void toMenu(String menu) {
        switch (menu) {
            case Variable.MENU_LOGOUT:
                SharedPreferences userPrefs = Misc.getSharedPreference(context, Variable.USER_PREFERENCE);
                Misc.clearPref(userPrefs);

                Intent main = ((Activity) context).getIntent();
                main.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                ((Activity) context).finish();
                context.startActivity(main);
                break;

            case Variable.MENU_PROFIL:
                Intent profile = new Intent(context, ProfileInformation.class);
                context.startActivity(profile);
                break;

            case Variable.MENU_ALAMAT:
                Intent address = new Intent(context, ProfileAddress.class);
                context.startActivity(address);
                break;

            case Variable.MENU_TELEPON:
                Intent phone = new Intent(context, ProfilePhone.class);
                context.startActivity(phone);
                break;

            case Variable.MENU_EMAIL:
                Intent email = new Intent(context, ProfileEmail.class);
                context.startActivity(email);
                break;

            case Variable.MENU_PASSWORD:
                Intent password = new Intent(context, ProfilePassword.class);
                context.startActivity(password);
                break;

            case Variable.MENU_LAIN:
                Intent otherMenu= new Intent(context, OtherMenu.class);
                context.startActivity(otherMenu);
                break;
        }
    }

    public class MenuProfileViewHolder extends RecyclerView.ViewHolder {
        public ImageView menuIcon;
        public TextView menuTitle;

        public MenuProfileViewHolder(LayoutInflater inflater, final ViewGroup parent) {
            super(inflater.inflate(R.layout.item_menu, parent, false));

            menuIcon = (ImageView) itemView.findViewById(R.id.menu_icon);
            menuTitle= (TextView) itemView.findViewById(R.id.menu_title);
        }
    }
}
