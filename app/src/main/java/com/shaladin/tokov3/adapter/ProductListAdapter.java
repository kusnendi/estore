package com.shaladin.tokov3.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.shaladin.tokov3.R;
import com.shaladin.tokov3.activity.products.ProductDetail;
import com.shaladin.tokov3.config.Variable;
import com.shaladin.tokov3.model.Product;
import com.shaladin.tokov3.utils.ProductsKeys;
import com.squareup.picasso.Picasso;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by EliteBook on 8/13/2016.
 */
public class ProductListAdapter extends RecyclerView.Adapter<ProductListAdapter.ProductViewHolder> {
    private List<Product> productList;
    private Context context;
    ArrayList<Product> productModel;

    public ProductListAdapter(Context context, List<Product> productList) {
        this.productList = productList;
        this.context = context;
    }

    @Override
    public ProductViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ProductViewHolder(LayoutInflater.from(parent.getContext()), parent);
    }

    @Override
    public void onBindViewHolder(ProductViewHolder holder, final int position) {
        Product product = productList.get(position);
        holder.productName.setText(product.getName());
        double price = Double.parseDouble(product.getPrice());
        DecimalFormat formatter = new DecimalFormat("#,###");
        holder.productPrice.setText("Rp. "+formatter.format(price));
        holder.productStock.setText("Stok: "+product.getAmount());
        Picasso.with(context).load(product.getImage()).placeholder(R.drawable.placeholder)
                .error(R.mipmap.ic_launcher)
                .fit()
                .into(holder.imageView);

        holder.itemView.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                Context context = v.getContext();

                Intent intent   = new Intent(context, ProductDetail.class);
                intent.putExtra(Variable.PRODUCT, productList.get(position).getName());
                intent.putExtra(Variable.PRODUCT_ID, productList.get(position).getId());
                intent.putExtra(Variable.PRICE, productList.get(position).getPrice());
                intent.putExtra(Variable.AMOUNT, productList.get(position).getAmount());
                intent.putExtra(ProductsKeys.KEY_PRODUCT_STEP, productList.get(position).getStep());
                intent.putExtra(ProductsKeys.KEY_PRODUCT_WEIGHT, productList.get(position).getWeight());
                intent.putExtra(Variable.IMAGE, productList.get(position).getImage());
                context.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return (null != productList ? productList.size() : 0);
    }

    public void setFilter(List<Product> filterProducts) {
        productModel = new ArrayList<>();
        productModel.addAll(filterProducts);
        notifyDataSetChanged();
    }

    public class ProductViewHolder extends RecyclerView.ViewHolder {
        public ImageView imageView;
        public TextView productName;
        public TextView productPrice;
        public TextView productStock;

        public ProductViewHolder (LayoutInflater inflater, final ViewGroup parent){
            super(inflater.inflate(R.layout.item_grid_catalog, parent, false));

            imageView   = (ImageView) itemView.findViewById(R.id.productImage);
            productName = (TextView) itemView.findViewById(R.id.productName);
            productPrice= (TextView) itemView.findViewById(R.id.productPrice);
            productStock= (TextView) itemView.findViewById(R.id.productStock);
        }
    }

}