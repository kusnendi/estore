package com.shaladin.tokov3.config;

/**
 * Created by EliteBook on 8/18/2016.
 */
public class ApiServices {
    public static final String URI   = "http://api.grosirbersama.co.id/api/";
//    public static final String URI   = "http://api.pasarikan.co/api/";
    public static final String AUTH  = URI+"auth";
    public static final String ORDER = URI+"orders";
    public static final String PRODUCT = URI+"products";
    public static final String COMPANY = URI+"company";
    public static final String PAYMENT = URI+"company-payments?company_id=";
    public static final String CHANGE_PASSWORD = URI+"changepassword/";
    public static final String SHIPPING_COST = URI+"shippingcost";
    public static final String PROVINSI = URI+"propinsi";
    public static final String KOTA= URI+"kabupaten?propinsi_id=";
    public static final String KECAMATAN = URI+"kota?kabupaten_id=";
    public static final String CUSTOMER = URI+"customer";
    public static final String PAGES = URI+"pages";
    public static final String PAYMENT_CONFIRMATION = URI+"confirmation";
    public static final String TOKEN = URI+"token";
    public static final String RESET_CODE = URI+"reset-code";
}
