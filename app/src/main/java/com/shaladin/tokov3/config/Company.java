package com.shaladin.tokov3.config;

/**
 * Created by EliteBook on 9/7/2016.
 */
public class Company {
    public static final String COMPANY_ID = "2022";
    public static final String COMPANY_NAME = "Pelangi Fashion";
    public static final String APP_PACKAGE  = "com.shaladin.pelangi";
    public static final boolean BASIC_AUTH  = false;
}