package com.shaladin.tokov3.config;

/**
 * Created by EliteBook on 5/17/2017.
 */

public class Variable {
    public static final String USER_PREFERENCE      = "UserPrefs";
    public static final String PRODUCTS_PREFERENCE  = "ProductsPrefs";
    public static final String STATES_PREFERENCE    = "StatesPrefs";
    public static final String PAYMENT_PREFERENCE   = "PaymentPrefs";
    public static final String CART_PREFS   = "CartPrefs";
    public static final String MENU_LOGOUT  = "Logout";
    public static final String MENU_ALAMAT  = "Alamat";
    public static final String MENU_TELEPON = "Telepon";
    public static final String MENU_PASSWORD= "Password";
    public static final String MENU_EMAIL   = "Email";
    public static final String MENU_LAIN    = "Lainnya";
    public static final String MENU_PROFIL  = "Profil";
    public static final String USER_ID   = "user_id";
    public static final String REALNAME  = "realname";
    public static final String FIRST_NAME= "firstname";
    public static final String LAST_NAME = "lastname";
    public static final String DATE_BIRTH= "date_birth";
    public static final String GENDER    = "gender";
    public static final String EMAIL     = "email";
    public static final String PHONE     = "phone";
    public static final String ADDRESS   = "address";
    public static final String STATE     = "state";
    public static final String STATE_ID  = "state_id";
    public static final String CITY      = "city";
    public static final String CITY_ID   = "city_id";
    public static final String DISTRICT  = "district";
    public static final String DISTRICT_ID = "district_id";
    public static final String ZIP       = "zipcode";
    public static final String SHIPPING  = "shipping";
    public static final String SHIPPING_ID  = "shipping_id";
    public static final String SHIPPING_COST= "shipping_cost";
    public static final String PAYMENT   = "payment";
    public static final String PAYMENT_ID= "payment_id";
    public static final String INSTRUCTIONS = "instructions";
    public static final String SUBTOTAL  = "subtotal";
    public static final String TOTAL     = "total";
    public static final String DISCOUNT  = "discount";
    public static final String NOTES     = "notes";
    public static final String KEY       = "key";
    public static final String PASSWORD  = "password";
    public static final String IS_AUTH   = "is_auth";
    public static final String FULLNAME  = "fullname";
    public static final String COMPANY_ID= "company_id";
    public static final String GROUP_ID  = "group_id";
    public static final String PRODUCTS  = "products";
    public static final String PRODUCT   = "product";
    public static final String PRODUCT_ID= "product_id";
    public static final String AMOUNT    = "amount";
    public static final String PRICE     = "price";
    public static final String IMAGE     = "image";
    public static final String RESPONSE  = "response";
    public static final String ERROR     = "error";
    public static final String MESSAGE   = "message";
    public static final String DATA      = "data";
    public static final String STATUS    = "status";
    public static final String isAUTH    = "isAuth";
}
