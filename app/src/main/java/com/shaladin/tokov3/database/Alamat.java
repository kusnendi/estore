package com.shaladin.tokov3.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import com.shaladin.tokov3.model.Address;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by EliteBook on 8/26/2016.
 */
public class Alamat {
    //Database field
    private SQLiteDatabase db;
    private SQLiteHelper helper;
    private String[] allColumns = {helper.COLUMN_ID, helper.COLUMN_NAME, helper.COLUMN_USER_ID, helper.COLUMN_IS_PRIMARY,
    helper.COLUMN_FIRSTNAME, helper.COLUMN_LASTNAME, helper.COLUMN_ADDRESS, helper.COLUMN_COUNTRY, helper.COLUMN_PROVINCY_ID,
    helper.COLUMN_PROVINCY_NAME, helper.COLUMN_KABUPATEN_ID, helper.COLUMN_KABUPATEN_NAME, helper.COLUMN_CITY_ID, helper.COLUMN_CITY_NAME,
    helper.COLUMN_PHONE, helper.COLUMN_POSTCODE};

    public Alamat(Context context) {
        helper = new SQLiteHelper(context);
    }

    public void open() throws SQLException {
        db = helper.getWritableDatabase();
    }

    public void close() {
        helper.close();
    }

    public long create(Address address) {
        int isPrimary;
        Cursor cursor = db.query(helper.TABLE_USER_ADDRESS, allColumns, helper.COLUMN_USER_ID + " = " +
                address.getUserId(), null, null, null, null);

        if(cursor.getCount() > 0) isPrimary = 0;
        else isPrimary = 1;

        ContentValues values = new ContentValues();
        values.put(helper.COLUMN_NAME, address.getName());
        values.put(helper.COLUMN_USER_ID, address.getUserId());
        values.put(helper.COLUMN_IS_PRIMARY, isPrimary);
        values.put(helper.COLUMN_FIRSTNAME, address.getRealName());
        values.put(helper.COLUMN_LASTNAME, address.getRealName());
        values.put(helper.COLUMN_ADDRESS, address.getStreetAddress());
        values.put(helper.COLUMN_COUNTRY, "ID");
        values.put(helper.COLUMN_PROVINCY_ID, address.getStateId());
        values.put(helper.COLUMN_PROVINCY_NAME, address.getStateName());
        values.put(helper.COLUMN_KABUPATEN_ID, address.getCityId());
        values.put(helper.COLUMN_KABUPATEN_NAME, address.getCityName());
        values.put(helper.COLUMN_CITY_ID, address.getDistrictId());
        values.put(helper.COLUMN_CITY_NAME, address.getDistrictName());
        values.put(helper.COLUMN_PHONE, address.getPhoneNumber());
        values.put(helper.COLUMN_POSTCODE, address.getPostCode());

        long id = db.insert(helper.TABLE_USER_ADDRESS, null, values);
        return ((id != 0) ? id : null);
    }

    public long update(Address address, long id) {
        ContentValues values = new ContentValues();
        values.put(helper.COLUMN_NAME, address.getName());
        values.put(helper.COLUMN_FIRSTNAME, address.getRealName());
        values.put(helper.COLUMN_LASTNAME, address.getRealName());
        values.put(helper.COLUMN_ADDRESS, address.getStreetAddress());
        values.put(helper.COLUMN_PROVINCY_ID, address.getStateId());
        values.put(helper.COLUMN_PROVINCY_NAME, address.getStateName());
        values.put(helper.COLUMN_KABUPATEN_ID, address.getCityId());
        values.put(helper.COLUMN_KABUPATEN_NAME, address.getCityName());
        values.put(helper.COLUMN_CITY_ID, address.getDistrictId());
        values.put(helper.COLUMN_CITY_NAME, address.getDistrictName());
        values.put(helper.COLUMN_PHONE, address.getPhoneNumber());
        values.put(helper.COLUMN_POSTCODE, address.getPostCode());

        db.update(helper.TABLE_USER_ADDRESS, values, helper.COLUMN_ID+" = "
        +id, null);
        db.close();

        return id;
    }

    public Address getById(long id) {
        Cursor cursor = db.query(helper.TABLE_USER_ADDRESS, allColumns, helper.COLUMN_ID+" = "
        +id, null, null, null, null);

        if(cursor.getCount() != 0) {
            cursor.moveToFirst();
            Address address = cursorToAddress(cursor);
            return address;
        } else {
            return null;
        }
    }

    public List<Address> getByUserId(int userId) {
        List<Address> addresses = new ArrayList<>();

        Cursor cursor = db.query(helper.TABLE_USER_ADDRESS, allColumns, helper.COLUMN_USER_ID + " = " +
                userId, null, null, null, null);

        if(cursor.getCount() > 0) {
            while (!cursor.isLast()) {
                cursor.moveToNext();
                addresses.add(cursorToAddress(cursor));
            }

            cursor.close();
            db.close();
        }

        return addresses;
    }

    public void delete(long id) {
        db.delete(helper.TABLE_USER_ADDRESS, helper.COLUMN_ID+" = "+id, null);
        db.close();
    }

    private Address cursorToAddress(Cursor cursor) {
        Address address = new Address();
        address.setId(cursor.getLong(cursor.getColumnIndex(helper.COLUMN_ID)));
        address.setName(cursor.getString(cursor.getColumnIndex(helper.COLUMN_NAME)));
        address.setUserId(cursor.getInt(cursor.getColumnIndex(helper.COLUMN_USER_ID)));
        address.setPrimary(cursor.getInt(cursor.getColumnIndex(helper.COLUMN_IS_PRIMARY)));
        address.setRealName(cursor.getString(cursor.getColumnIndex(helper.COLUMN_FIRSTNAME)));
        address.setStreetAddress(cursor.getString(cursor.getColumnIndex(helper.COLUMN_ADDRESS)));
        address.setStateId(cursor.getInt(cursor.getColumnIndex(helper.COLUMN_PROVINCY_ID)));
        address.setStateName(cursor.getString(cursor.getColumnIndex(helper.COLUMN_PROVINCY_NAME)));
        address.setCityId(cursor.getInt(cursor.getColumnIndex(helper.COLUMN_KABUPATEN_ID)));
        address.setCityName(cursor.getString(cursor.getColumnIndex(helper.COLUMN_KABUPATEN_NAME)));
        address.setDistrictId(cursor.getInt(cursor.getColumnIndex(helper.COLUMN_CITY_ID)));
        address.setDistrictName(cursor.getString(cursor.getColumnIndex(helper.COLUMN_CITY_NAME)));
        address.setPhoneNumber(cursor.getString(cursor.getColumnIndex(helper.COLUMN_PHONE)));
        address.setPostCode(cursor.getString(cursor.getColumnIndex(helper.COLUMN_POSTCODE)));
        return address;
    }
}
