package com.shaladin.tokov3.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import com.shaladin.tokov3.model.Cart;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by EliteBook on 8/15/2016.
 */
public class Carts {
    //Database field
    private SQLiteDatabase db;
    private SQLiteHelper helper;
    private String[] allColumns = {helper.COLUMN_ID, helper.COLUMN_PRODUCT_ID, helper.COLUMN_PRODUCT, helper.COLUMN_IMAGE,
    helper.COLUMN_PRICE, helper.COLUMN_AMOUNT, helper.COLUMN_WEIGHT, helper.COLUMN_NOTES, helper.COLUMN_SUBTOTAL};

    public Carts(Context context) {
        helper = new SQLiteHelper(context);
    }

    public void open() throws SQLException {
        db = helper.getWritableDatabase();
    }

    public void close() {
        helper.close();
    }

    public Cart createCart(int productId, String product, String image, double price, int amount, double weight, double subtotal)
    {
        ContentValues values = new ContentValues();
        values.put(helper.COLUMN_PRODUCT_ID, productId);
        values.put(helper.COLUMN_PRODUCT, product);
        values.put(helper.COLUMN_IMAGE, image);
        values.put(helper.COLUMN_PRICE, price);
        values.put(helper.COLUMN_AMOUNT, amount);
        values.put(helper.COLUMN_WEIGHT, weight);
        values.put(helper.COLUMN_NOTES, "");
        values.put(helper.COLUMN_SUBTOTAL, subtotal);

        long insertedId = db.insert(helper.TABLE_CARTS, null, values);
        Cursor cursor = db.query(helper.TABLE_CARTS, allColumns, helper.COLUMN_ID + " = "
            + insertedId, null, null, null, null);

        cursor.moveToFirst();
        Cart newCart = cursorToCart(cursor);
        cursor.close();
        db.close();
        return newCart;
    }

    public void updateQuantity(double Price, int Qty, int productId) {
        ContentValues values = new ContentValues();
        values.put(helper.COLUMN_AMOUNT, Qty);
        values.put(helper.COLUMN_SUBTOTAL, Price*Qty);
        db.update(helper.TABLE_CARTS, values, helper.COLUMN_PRODUCT_ID+" = "+productId, null);
        db.close();
    }

    public void updateNotes(String note, int productId) {
        ContentValues values = new ContentValues();
        values.put(helper.COLUMN_NOTES, note);
        db.update(helper.TABLE_CARTS, values, helper.COLUMN_PRODUCT_ID+" = "+productId, null);
        db.close();
    }

    public void deleteCart(Cart cart) {
        long id = cart.getId();
        db.delete(helper.TABLE_CARTS, helper.COLUMN_ID + " = " + id, null);
        db.close();
    }

    public void clearCart() {
        db.delete(helper.TABLE_CARTS, null, null);
        db.close();
    }

    public List<Cart> allCarts() {
        List<Cart> carts = new ArrayList<>();

        Cursor cursor = db.query(helper.TABLE_CARTS, allColumns, null, null, null, null, null);
        if(cursor.getCount() > 0)
        {
            while (!cursor.isLast()) {
                cursor.moveToNext();
                carts.add(cursorToCart(cursor));
            }
            cursor.close();
            db.close();
            return carts;
        } else {
            db.close();
            return null;
        }
    }

    private Cart cursorToCart(Cursor cursor) {
        Cart cart = new Cart();
        cart.setId(cursor.getLong(cursor.getColumnIndex(helper.COLUMN_ID)));
        cart.setProductId(cursor.getInt(cursor.getColumnIndex(helper.COLUMN_PRODUCT_ID)));
        cart.setProduct(cursor.getString(cursor.getColumnIndex(helper.COLUMN_PRODUCT)));
        cart.setImage(cursor.getString(cursor.getColumnIndex(helper.COLUMN_IMAGE)));
        cart.setPrice(cursor.getDouble(cursor.getColumnIndex(helper.COLUMN_PRICE)));
        cart.setAmount(cursor.getInt(cursor.getColumnIndex(helper.COLUMN_AMOUNT)));
        cart.setWeight(cursor.getDouble(cursor.getColumnIndex(helper.COLUMN_WEIGHT)));
        cart.setNote(cursor.getString(cursor.getColumnIndex(helper.COLUMN_NOTES)));
        cart.setSubtotal(cursor.getDouble(cursor.getColumnIndex(helper.COLUMN_SUBTOTAL)));
        return cart;
    }
}
