package com.shaladin.tokov3.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import com.shaladin.store.model.OrderDetail;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by EliteBook on 8/24/2016.
 */
public class OrderDetails {
    //database fields
    SQLiteDatabase db;
    SQLiteHelper helper;
    String[] allColumns = {helper.COLUMN_ID, helper.COLUMN_ORDER_ID, helper.COLUMN_PRODUCT_ID, helper.COLUMN_PRODUCT,
            helper.COLUMN_PRICE, helper.COLUMN_AMOUNT, helper.COLUMN_IMAGE};

    public OrderDetails(Context context) {
        helper = new SQLiteHelper(context);
    }

    public void open() throws SQLException {
        db = helper.getWritableDatabase();
    }

    public void close() {
        db.close();
        helper.close();
    }

    public boolean createOrderDetail(OrderDetail orderDetail, int orderId) {
        ContentValues values = new ContentValues();
        values.put(helper.COLUMN_ORDER_ID, orderId);
        values.put(helper.COLUMN_PRODUCT_ID, orderDetail.getProductId());
        values.put(helper.COLUMN_PRODUCT, orderDetail.getProduct());
        values.put(helper.COLUMN_PRICE, orderDetail.getPrice());
        values.put(helper.COLUMN_AMOUNT, orderDetail.getAmount());
        values.put(helper.COLUMN_IMAGE, orderDetail.getImage());

        long id = db.insert(helper.TABLE_ORDERS_DETAIL, null, values);

        if(id != 0) return true;
        else return false;
    }

    public boolean clearDetail(int orderId) {
        db.delete(helper.TABLE_ORDERS_DETAIL, helper.COLUMN_ORDER_ID + " = " + orderId, null);
        db.close();
        return true;
    }

    public List<OrderDetail> getDetailOrderId(int orderId) {
        List<OrderDetail> details = new ArrayList<>();

        Cursor cursor = db.query(helper.TABLE_ORDERS_DETAIL, allColumns, helper.COLUMN_ORDER_ID+" = "+orderId, null, null, null, null);
        if(cursor.getCount() > 0)
        {
            while (!cursor.isLast()) {
                cursor.moveToNext();
                details.add(cursorToOrderDetail(cursor));
            }
            cursor.close();
            return details;
        } else {
            return null;
        }
    }

    private OrderDetail cursorToOrderDetail(Cursor cursor) {
        OrderDetail orderDetail = new OrderDetail();
        orderDetail.setId(cursor.getLong(cursor.getColumnIndex(helper.COLUMN_ID)));
        orderDetail.setOrderId(cursor.getInt(cursor.getColumnIndex(helper.COLUMN_ORDER_ID)));
        orderDetail.setProductId(cursor.getInt(cursor.getColumnIndex(helper.COLUMN_PRODUCT_ID)));
        orderDetail.setProduct(cursor.getString(cursor.getColumnIndex(helper.COLUMN_PRODUCT)));
        orderDetail.setAmount(cursor.getInt(cursor.getColumnIndex(helper.COLUMN_AMOUNT)));
        orderDetail.setPrice(cursor.getDouble(cursor.getColumnIndex(helper.COLUMN_PRICE)));
        orderDetail.setImage(cursor.getString(cursor.getColumnIndex(helper.COLUMN_IMAGE)));
        return orderDetail;
    }
}
