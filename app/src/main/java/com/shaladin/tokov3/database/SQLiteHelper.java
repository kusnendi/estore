package com.shaladin.tokov3.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * Created by EliteBook on 8/15/2016.
 */
public class SQLiteHelper extends SQLiteOpenHelper {
    public static final String TABLE_CARTS = "carts";
    public static final String TABLE_USERS = "users";
    public static final String TABLE_USER_AUTH = "user_auth";
    public static final String TABLE_USER_ADDRESS = "user_address";
    public static final String TABLE_ORDERS= "orders";
    public static final String TABLE_ORDERS_DETAIL = "orders_detail";
    public static final String TABLE_PAYMENT_CONFIRMATIONS = "payment_confirmations";
    public static final String COLUMN_ID   = "id";
    public static final String COLUMN_PRODUCT = "product";
    public static final String COLUMN_PRODUCT_ID = COLUMN_PRODUCT+"_"+COLUMN_ID;
    public static final String COLUMN_IMAGE = "image";
    public static final String COLUMN_PRICE = "price";
    public static final String COLUMN_AMOUNT= "amount";
    public static final String COLUMN_WEIGHT= "weight";
    public static final String COLUMN_SUBTOTAL = "subtotal";
    public static final String COLUMN_TOTAL   = "total";
    public static final String COLUMN_SHIPPING_COST = "shipping_cost";
    public static final String COLUMN_DISCOUNT= "discount";
    public static final String COLUMN_TIMESTAMP = "timestamp";
    public static final String COLUMN_PAYMENT_ID= "payment_id";
    public static final String COLUMN_SHIPPING_ID = "shipping_id";
    public static final String COLUMN_NOTES = "notes";
    public static final String COLUMN_ORDER_ID= "order_id";
    public static final String COLUMN_USER_ID = "user_id";
    public static final String COLUMN_EMAIL = "email";
    public static final String COLUMN_NAME = "name";
    public static final String COLUMN_FIRSTNAME = "firstname";
    public static final String COLUMN_LASTNAME  = "lastname";
    public static final String COLUMN_COUNTRY = "country";
    public static final String COLUMN_PROVINCY_ID = "provincy_id";
    public static final String COLUMN_PROVINCY_NAME = "provincy_name";
    public static final String COLUMN_KABUPATEN_ID = "kabupaten_id";
    public static final String COLUMN_KABUPATEN_NAME = "kabupaten_name";
    public static final String COLUMN_CITY_ID = "city_id";
    public static final String COLUMN_CITY_NAME = "city_name";
    public static final String COLUMN_ADDRESS = "address";
    public static final String COLUMN_ADDRESS_TYPE = "address_type";
    public static final String COLUMN_PHONE = "phone";
    public static final String COLUMN_POSTCODE = "postcode";
    public static final String COLUMN_ADDRESS_ID  = "address_id";
    public static final String COLUMN_IS_DEFAULT  = "is_default";
    public static final String COLUMN_IS_PRIMARY  = "is_primary";
    public static final String COLUMN_B_FIRSTNAME = "b_firstname";
    public static final String COLUMN_B_LASTNAME  = "b_lastname";
    public static final String COLUMN_B_ADDRESS   = "b_address";
    public static final String COLUMN_B_COUNTRY   = "b_country";
    public static final String COLUMN_B_STATE_ID  = "b_state_id";
    public static final String COLUMN_B_STATE     = "b_state";
    public static final String COLUMN_B_CITY_ID   = "b_city_id";
    public static final String COLUMN_B_CITY      = "b_city";
    public static final String COLUMN_B_DISTRICT_ID  = "b_district_id";
    public static final String COLUMN_B_DISTRICT  = "b_district";
    public static final String COLUMN_B_PHONE     = "b_phone";
    public static final String COLUMN_B_POSTCODE  = "b_postcode";
    public static final String COLUMN_S_FIRSTNAME = "s_firstname";
    public static final String COLUMN_S_LASTNAME  = "s_lastname";
    public static final String COLUMN_S_ADDRESS   = "s_address";
    public static final String COLUMN_S_COUNTRY   = "s_country";
    public static final String COLUMN_S_STATE_ID  = "s_state_id";
    public static final String COLUMN_S_STATE     = "s_state";
    public static final String COLUMN_S_CITY_ID   = "s_city_id";
    public static final String COLUMN_S_CITY      = "s_city";
    public static final String COLUMN_S_DISTRICT_ID  = "s_district_id";
    public static final String COLUMN_S_DISTRICT  = "s_district";
    public static final String COLUMN_S_PHONE     = "s_phone";
    public static final String COLUMN_S_POSTCODE  = "s_postcode";
    public static final String COLUMN_STATUS = "status";
    public static final String COLUMN_SECRET_KEY = "secret_key";
    public static final String COLUMN_PASSWORD= "password";
    public static final String COLUMN_IS_AUTH = "is_auth";
    public static final String COLUMN_ACCOUNT_NAME = "account_name";
    public static final String COLUMN_ACCOUNT_NO = "account_number";
    public static final String COLUMN_SENDER_BANK= "sender_bank";
    public static final String COLUMN_TO_BANK = "to_bank";
    public static final String COLUMN_RECEIPT = "receipt";
    public static final String COLUMN_DATE_TRANSFER = "date_transfer";
    public static final String COLUMN_COMPANY_ID = "company_id";

    private static final String DATABASE_NAME = "data.db";
    private static final int DATABASE_VERSION = 9;

    public static String CREATE_TABLE_PAYMENT_CONFIRMATIONS = "create table "+TABLE_PAYMENT_CONFIRMATIONS+"("+COLUMN_ID+" integer primary key autoincrement, "
            +COLUMN_ORDER_ID+" integer not null, "+COLUMN_COMPANY_ID+" integer not null, "+COLUMN_ACCOUNT_NAME+" text not null, "+COLUMN_ACCOUNT_NO+" integer not null, "
            +COLUMN_SENDER_BANK+" text not null, "+COLUMN_TO_BANK+" text not null, "+COLUMN_AMOUNT+" real not null, "+COLUMN_RECEIPT+" integer not null, "
            +COLUMN_DATE_TRANSFER+" text not null);";

    public static String CREATE_TABLE_CARTS = "create table "+TABLE_CARTS+"("+COLUMN_ID+" integer primary key autoincrement, "
            +COLUMN_PRODUCT_ID+" integer not null, "+COLUMN_PRODUCT+" text not null, "+COLUMN_IMAGE+" text not null, "
            +COLUMN_PRICE+" real not null, "+COLUMN_AMOUNT+" integer not null, "+COLUMN_WEIGHT+" real not null, "
            +COLUMN_NOTES+" text not null, "+COLUMN_SUBTOTAL+" real not null);";

    public static String CREATE_TABLE_USERS = "create table "+TABLE_USERS+"("+COLUMN_ID+" integer primary key autoincrement, "
            +COLUMN_USER_ID+" integer not null, "+COLUMN_EMAIL+" text not null, "+COLUMN_FIRSTNAME+" text not null, "
            +COLUMN_LASTNAME+" text not null, "+COLUMN_COUNTRY+" text not null, "+COLUMN_PROVINCY_ID+" integer not null, "
            +COLUMN_PROVINCY_NAME+" text not null, "+COLUMN_KABUPATEN_ID+" integer not null, "+COLUMN_KABUPATEN_NAME
            +" text not null, "+COLUMN_CITY_ID+" integer not null, "+COLUMN_CITY_NAME+" text not null, "+COLUMN_ADDRESS
            +" text not null, "+COLUMN_PHONE+" text not null, "+COLUMN_POSTCODE+" text not null, "+COLUMN_STATUS
            +" text not null);";

    public static String CREATE_TABLE_USER_AUTH = "create table "+TABLE_USER_AUTH+"("+COLUMN_ID+" integer primary key autoincrement, "
            +COLUMN_USER_ID+" integer not null, "+COLUMN_SECRET_KEY+" text not null, "+COLUMN_PASSWORD+" text not null, "
            +COLUMN_IS_AUTH+" integer not null);";

    public static String CREATE_TABLE_USER_ADDRESS = "create table "+TABLE_USER_ADDRESS+"("+COLUMN_ID+" integer primary key autoincrement, "
            +COLUMN_NAME+" text not null, "+COLUMN_USER_ID+" integer not null, "+COLUMN_IS_PRIMARY+" integer not null, "
            +COLUMN_FIRSTNAME+" text not null, "+COLUMN_LASTNAME+" text not null, "+COLUMN_ADDRESS+" text not null, "
            +COLUMN_COUNTRY+" text not null, "+COLUMN_PROVINCY_ID+" integer not null, "+COLUMN_PROVINCY_NAME+" text not null, "
            +COLUMN_KABUPATEN_ID+" integer not null, "+COLUMN_KABUPATEN_NAME+" text not null, "+COLUMN_CITY_ID+" integer not null, "
            +COLUMN_CITY_NAME+" text not null, "+COLUMN_PHONE+" text not null, "+COLUMN_POSTCODE+" text not null);";

    public static String CREATE_TABLE_ORDERS = "create table "+TABLE_ORDERS+"("+COLUMN_ID+" integer primary key autoincrement, "
            +COLUMN_ORDER_ID+" integer not null, "+COLUMN_USER_ID+" integer not null, "+COLUMN_SUBTOTAL+" real not null, "
            +COLUMN_DISCOUNT+" real not null, "+COLUMN_SHIPPING_COST+" real not null, "+COLUMN_TOTAL+" real not null, "
            +COLUMN_TIMESTAMP+" integer not null, "+COLUMN_PAYMENT_ID+" integer not null, "+COLUMN_SHIPPING_ID+" integer not null, "
            +COLUMN_B_FIRSTNAME+" text not null, "+COLUMN_B_LASTNAME+" text not null, "+COLUMN_B_STATE_ID+" text not null, "
            +COLUMN_B_STATE+" text not null, "+COLUMN_B_CITY_ID+" text not null, "+COLUMN_B_CITY+" text not null, "
            +COLUMN_B_DISTRICT_ID+" text not null, "+COLUMN_B_DISTRICT+" text not null, "+COLUMN_B_ADDRESS+" text not null, "
            +COLUMN_B_PHONE+" text not null, "+COLUMN_B_POSTCODE+" text not null, "+COLUMN_S_FIRSTNAME+" text not null, "
            +COLUMN_S_LASTNAME+" text not null, "+COLUMN_S_STATE_ID+" text not null, "+COLUMN_S_STATE+" text not null, "
            +COLUMN_S_CITY_ID+" text not null, "+COLUMN_S_CITY+" text not null, "+COLUMN_S_DISTRICT_ID+" text not null, "
            +COLUMN_S_DISTRICT+" text not null, "+COLUMN_S_ADDRESS+" text not null, "+COLUMN_S_PHONE+" text not null, "
            +COLUMN_S_POSTCODE+" text not null, "+COLUMN_NOTES+" text not null);";

    public static String CREATE_TABLE_ORDERS_DETAIL = "create table "+TABLE_ORDERS_DETAIL+"("+COLUMN_ID+" integer primary key autoincrement, "
            +COLUMN_ORDER_ID+" integer not null, "+COLUMN_PRODUCT_ID+" integer not null, "+COLUMN_PRODUCT+" text not null, "
            +COLUMN_PRICE+" real not null, "+COLUMN_AMOUNT+" integer not null, "+COLUMN_IMAGE+" text not null);";

    public SQLiteHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE_CARTS);
        db.execSQL(CREATE_TABLE_USERS);
        db.execSQL(CREATE_TABLE_USER_AUTH);
        db.execSQL(CREATE_TABLE_USER_ADDRESS);
        db.execSQL(CREATE_TABLE_ORDERS);
        db.execSQL(CREATE_TABLE_ORDERS_DETAIL);
        db.execSQL(CREATE_TABLE_PAYMENT_CONFIRMATIONS);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.w(SQLiteHelper.class.getName(), "Upgrading database from version "+oldVersion+" to "+newVersion);
        db.execSQL("DROP TABLE IF EXISTS "+TABLE_CARTS);
        db.execSQL(CREATE_TABLE_CARTS);
        db.execSQL("DROP TABLE IF EXISTS "+TABLE_USERS);
        db.execSQL(CREATE_TABLE_USERS);
        db.execSQL("DROP TABLE IF EXISTS "+TABLE_USER_AUTH);
        db.execSQL(CREATE_TABLE_USER_AUTH);
        db.execSQL("DROP TABLE IF EXISTS "+TABLE_USER_ADDRESS);
        db.execSQL(CREATE_TABLE_USER_ADDRESS);
        db.execSQL("DROP TABLE IF EXISTS "+TABLE_ORDERS);
        db.execSQL(CREATE_TABLE_ORDERS);
        db.execSQL("DROP TABLE IF EXISTS "+TABLE_ORDERS_DETAIL);
        db.execSQL(CREATE_TABLE_ORDERS_DETAIL);
        db.execSQL("DROP TABLE IF EXISTS "+TABLE_PAYMENT_CONFIRMATIONS);
        db.execSQL(CREATE_TABLE_PAYMENT_CONFIRMATIONS);
    }

}
