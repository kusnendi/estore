package com.shaladin.tokov3.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import com.shaladin.tokov3.model.Auth;

/**
 * Created by EliteBook on 8/18/2016.
 */
public class UserAuth {
    //database fields
    private SQLiteDatabase db;
    private SQLiteHelper helper;
    private String[] allColumns = {helper.COLUMN_ID, helper.COLUMN_USER_ID, helper.COLUMN_SECRET_KEY, helper.COLUMN_PASSWORD, helper.COLUMN_IS_AUTH};

    public UserAuth(Context context) {
        helper = new SQLiteHelper(context);
    }

    public void open() throws SQLException {
        db = helper.getWritableDatabase();
    }

    public void close() {
        helper.close();
    }

    public Auth storeAuth(int userId, String secretKey, String password, int isAuth) {
        ContentValues values = new ContentValues();
        values.put(helper.COLUMN_USER_ID, userId);
        values.put(helper.COLUMN_SECRET_KEY, secretKey);
        values.put(helper.COLUMN_PASSWORD, password);
        values.put(helper.COLUMN_IS_AUTH, isAuth);

        long insertedId = db.insert(helper.TABLE_USER_AUTH, null, values);
        Cursor cursor = db.query(helper.TABLE_USER_AUTH, allColumns, helper.COLUMN_ID + " = "
        + insertedId, null, null, null, null);
        cursor.moveToFirst();
        Auth userAuth = cursorToUserAuth(cursor);
        return userAuth;
    }

    public void updatePassword(String password, int uid) {
        ContentValues values = new ContentValues();
        values.put(helper.COLUMN_PASSWORD, password);
        db.update(helper.TABLE_USER_AUTH, values, helper.COLUMN_USER_ID+" = "+uid, null);
        db.close();
    }

    public Auth isAuth() {
        Cursor cursor = db.query(helper.TABLE_USER_AUTH, allColumns, null, null, null, null, null);
        if(cursor.getCount() > 0)
        {
            cursor.moveToFirst();
            Auth userAuth = cursorToUserAuth(cursor);
            cursor.close(); db.close();
            return userAuth;
        }
        else
        {
            cursor.close(); db.close();
            return null;
        }
    }

    public void unAuth(){
        db.delete(helper.TABLE_USER_AUTH, null, null);
        db.close();
    }

    private Auth cursorToUserAuth(Cursor cursor) {
        Auth userAuth = new Auth();
        userAuth.setId(cursor.getLong(0));
        userAuth.setUserId(cursor.getInt(1));
        userAuth.setSecretKey(cursor.getString(2));
        userAuth.setPassword(cursor.getString(3));
        userAuth.setIsAuth(1);
        return userAuth;
    }

}
