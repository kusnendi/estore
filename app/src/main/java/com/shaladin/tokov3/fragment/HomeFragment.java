package com.shaladin.tokov3.fragment;


import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.TextSliderView;
import com.shaladin.tokov3.R;
import com.shaladin.tokov3.adapter.ProductListAdapter;
import com.shaladin.tokov3.config.Company;
import com.shaladin.tokov3.model.Product;
import com.shaladin.tokov3.http.Requests;
import com.shaladin.tokov3.utils.InternetConnection;
import com.shaladin.tokov3.utils.Misc;
import com.shaladin.tokov3.utils.ProductsKeys;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class HomeFragment extends Fragment {

    private SliderLayout sliderView;
    private List<Product> productList = new ArrayList<>();
    private List<Product> popularList = new ArrayList<>();
    private ProductListAdapter adapter;
    private ProductListAdapter popularAdapter;
    private RecyclerView recyclerView;
    private RecyclerView popularCycle;

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PRODUCTS = "products";
    // TODO: Rename and change types of parameters
    private String mProducts;

    SharedPreferences ProductsPrefs;

    public HomeFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ProductsPrefs = Misc.getSharedPreference(getContext(), "ProductsPrefs");
        if(!ProductsPrefs.contains("products"))
        {
            final String url = "http://api.grosirbersama.co.id/api/products?items_per_page=10&company_id=" + Company.COMPANY_ID;
            if(InternetConnection.checkConnection(getContext())) {
                new AsyncJob().execute(url);
            } else {
                Toast.makeText(getContext(), "Internet Connection Not Available", Toast.LENGTH_LONG)
                        .show();
            }
        }
    }

    @Override
    public void onStop() {
        sliderView.stopAutoCycle();
        super.onStop();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_home, container, false);

        HashMap<String,String> images_map = new HashMap<String, String>();
        images_map.put("Hannibal", "http://static2.hypable.com/wp-content/uploads/2013/12/hannibal-season-2-release-date.jpg");
        images_map.put("Big Bang Theory", "http://tvfiles.alphacoders.com/100/hdclearart-10.png");
        images_map.put("House of Cards", "http://cdn3.nflximg.net/images/3093/2043093.jpg");
        images_map.put("Game of Thrones", "http://images.boomsbeat.com/data/images/full/19640/game-of-thrones-season-4-jpg.jpg");

        sliderView = (SliderLayout) view.findViewById(R.id.bannerPromotions);
        for (String name : images_map.keySet()) {
            TextSliderView textSliderView = new TextSliderView(view.getContext());
            //initial textSliderView layout
            textSliderView.image(images_map.get(name))
                    .setScaleType(BaseSliderView.ScaleType.Fit);

            textSliderView.bundle(new Bundle());
            textSliderView.getBundle()
                    .putString("extra", name);

            sliderView.addSlider(textSliderView);
        }

        //Latest Product Showcase
        recyclerView = (RecyclerView) view.findViewById(R.id.mainRecyler);
        GridLayoutManager gridLayoutManager = new GridLayoutManager(getActivity(), 1, GridLayoutManager.HORIZONTAL, false);
        recyclerView.setLayoutManager(gridLayoutManager);
        recyclerView.setHasFixedSize(true);
        recyclerView.setNestedScrollingEnabled(false);
        adapter = new ProductListAdapter(recyclerView.getContext(), productList);
        recyclerView.setAdapter(adapter);

        //Popular Product Showcase
        popularCycle = (RecyclerView) view.findViewById(R.id.PopularRecycle);
        GridLayoutManager gridLayoutPopular = new GridLayoutManager(getActivity(), 1, GridLayoutManager.HORIZONTAL, false);
        popularCycle.setLayoutManager(gridLayoutPopular);
        popularCycle.setHasFixedSize(true);
        popularCycle.setNestedScrollingEnabled(false);
        popularAdapter = new ProductListAdapter(view.getContext(), productList);
        popularCycle.setAdapter(popularAdapter);

        if(ProductsPrefs.contains("products"))
        {
            try
            {
                JSONObject productsObject = new JSONObject(ProductsPrefs.getString("products", null));
                jsonObject(productsObject);
            }
            catch (JSONException je) {
                je.printStackTrace();
            }
        }

        // Inflate the layout for this fragment
        return view;
    }

    private class AsyncJob extends AsyncTask<String, Void, Void> {
        ProgressDialog dialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            /**
             * Progress dialog for User Interaction
             */
            dialog = new ProgressDialog(getContext());
            dialog.setMessage("Mohon tunggu...");
            dialog.show();
        }

        @Override
        protected Void doInBackground(String... params) {
            try {
                String url = params[0];
                JSONObject jsonObject = Requests.getData(url);
                if(jsonObject != null) {
                    jsonObject(jsonObject);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            dialog.dismiss();
        }
    }

    private void jsonObject(JSONObject object) {
        try {
            boolean error = object.getBoolean("error");

            if(!error) {
                JSONObject objectJson = object.getJSONObject("response");
                JSONArray products = objectJson.getJSONArray("products");
                int sizeArray = products.length();
                if(sizeArray > 0) {

                    for(int jIndex = 0; jIndex < sizeArray; jIndex++) {

                        Product model = new Product();
                        JSONObject product = products.getJSONObject(jIndex);
                        String productId = product.getString(ProductsKeys.KEY_PRODUCT_ID);
                        String name = product.getString(ProductsKeys.KEY_PRODUCT_NAME);
                        String price= product.getString(ProductsKeys.KEY_PRODUCT_PRICE);
                        String amount = product.getString("amount");
                        String step = product.getString(ProductsKeys.KEY_PRODUCT_STEP);
                        String weight = product.getString(ProductsKeys.KEY_PRODUCT_WEIGHT);
                        JSONObject main_pair = product.getJSONObject(ProductsKeys.KEY_PRODUCT_MAIN_PAIR);
                        JSONObject detailed  = main_pair.getJSONObject(ProductsKeys.KEY_PRODUCT_MAIN_PAIR_DETAILED);
                        String image = detailed.getString(ProductsKeys.KEY_PRODUCT_IMAGE_PATH);

                        model.setId(productId);
                        model.setName(name);
                        model.setPrice(price);
                        model.setAmount(amount);
                        model.setStep(step);
                        model.setWeight(weight);
                        model.setImage(image);
                        productList.add(model);
                        adapter.notifyItemInserted(jIndex);
                        popularAdapter.notifyItemInserted(jIndex);
                    }
                }
            } else {
                String message = object.getString("message");
                System.out.println(message);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

}
