package com.shaladin.tokov3.fragment;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.shaladin.tokov3.R;
import com.shaladin.tokov3.adapter.MenuProfileAdapter;
import com.shaladin.tokov3.utils.DividerItemDecoration;
import com.shaladin.tokov3.utils.Misc;

import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * to handle interaction events.
 * Use the {@link ProfileFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ProfileFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
//    private List<String, String> menus = new ArrayList<String, String>();
    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    SharedPreferences UserPrefs;

    public static String[] menuTitles = {
            "Profil",
            "Alamat",
            "Telepon",
            "Email",
            "Password",
            "Lainnya",
            "Logout"
    };

    public static int[] menuIcons = {
            R.drawable.ic_identity_black_24dp,
            R.drawable.ic_address_black_24dp,
            R.drawable.ic_phone_android_black_24dp,
            R.drawable.ic_email_black_24dp,
            R.drawable.ic_lock_black_24dp,
            R.drawable.ic_list_black_24dp,
            R.drawable.ic_exit_black_24dp
    };

    public ProfileFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment ProfileFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ProfileFragment newInstance(String param1, String param2) {
        ProfileFragment fragment = new ProfileFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_profile, container, false);
        RecyclerView recycleView = (RecyclerView) view.findViewById(R.id.mainRecyler);
        MenuProfileAdapter profileAdapter = new MenuProfileAdapter(recycleView.getContext(), menuTitles, menuIcons);
        LinearLayoutManager layoutManager = new LinearLayoutManager(view.getContext());
        recycleView.setLayoutManager(layoutManager);
        recycleView.setHasFixedSize(true);
        recycleView.setAdapter(profileAdapter);
        RecyclerView.ItemDecoration itemDecoration = new
                DividerItemDecoration(recycleView.getContext(), DividerItemDecoration.VERTICAL_LIST);
        recycleView.addItemDecoration(itemDecoration);
        profileAdapter.notifyDataSetChanged();

        return view;
    }
}
