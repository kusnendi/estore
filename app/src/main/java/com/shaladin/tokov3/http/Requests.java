package com.shaladin.tokov3.http;

import android.support.annotation.NonNull;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * Created by EliteBook on 8/14/2016.
 */
public class Requests {
    public String url;
    private static final String TAG = "TAG";
    private static final String KEY_USER_ID = "user_id";
    private static MediaType JSON;

    public static JSONObject getData(String url) {
        try {
            OkHttpClient client = new OkHttpClient();
            Request request = new okhttp3.Request.Builder()
                    .url(url)
                    .build();
            Response response = client.newCall(request).execute();
            return new JSONObject(response.body().string());
        } catch (@NonNull IOException | JSONException e) {
            Log.e(TAG, "" + e.getLocalizedMessage());
        }
        return null;
    }

    public static String postData(String url, String json) {
        try {
            JSON = MediaType.parse("application/json; charset=utf-8");
            OkHttpClient client = new OkHttpClient();
            RequestBody body = RequestBody.create(JSON, json);
            Request request  = new okhttp3.Request.Builder()
                    .url(url)
                    .post(body)
                    .addHeader("Content-Type", "application/json")
                    .build();
            Response response = client.newCall(request).execute();
            return response.body().string();
        } catch (@NonNull IOException ie) {
            Log.e(TAG, "" + ie.getLocalizedMessage());
        }
        //Failed Processing Post Data to Server
        return null;
    }
}
