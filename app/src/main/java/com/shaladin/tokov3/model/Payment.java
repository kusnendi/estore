package com.shaladin.tokov3.model;

/**
 * Created by EliteBook on 8/19/2016.
 */
public class Payment {
    private int id;
    private int paymentId;
    private String payment;
    private String instructions;

    public int getId() {
        return id;
    }

    public void setId(int id){
        this.id = id;
    }

    public int getPaymentId() {
        return paymentId;
    }

    public void setPaymentId(int paymentId) {
        this.paymentId = paymentId;
    }

    public String getPayment() {
        return payment;
    }

    public void setPayment(String payment) {
        this.payment = payment;
    }

    public String getInstructions() {
        return instructions;
    }

    public void setInstructions(String instructions) {
        this.instructions = instructions;
    }
}
