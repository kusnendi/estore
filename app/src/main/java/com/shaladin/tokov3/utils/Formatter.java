package com.shaladin.tokov3.utils;

import java.text.DecimalFormat;

/**
 * Created by EliteBook on 4/17/2017.
 */

public class Formatter {
    public static String accounting(String amount, String symbol) {
        Double nominal = Double.parseDouble(amount);
        DecimalFormat decimalFormat = new DecimalFormat("#,###");
        return symbol + decimalFormat.format(nominal);
    }
}
