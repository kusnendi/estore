package com.shaladin.tokov3.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Environment;
import android.util.Base64;
import android.widget.Toast;

import com.shaladin.tokov3.config.Variable;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.io.File;
import java.io.FileOutputStream;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by EliteBook on 5/12/2017.
 */

public class Misc implements MediaScannerConnection.MediaScannerConnectionClient{
    private MediaScannerConnection mMs;
    private File mFile;
    private String imageUrl;

    private void ScanFile(Context context, File f) {
        mFile = f;
        mMs = new MediaScannerConnection(context, this);
        mMs.connect();
    }

    @Override
    public void onMediaScannerConnected() {
        mMs.scanFile(mFile.getAbsolutePath(), null);
    }

    @Override
    public void onScanCompleted(String path, Uri uri) {
        mMs.disconnect();
    }

    public void downloadImage(final Context context, String imageUrl) {
        Picasso.with(context).load(imageUrl).into(new Target() {
            @Override
            public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                try {
                    String root = Environment.getExternalStorageDirectory().toString();
                    File myDir = new File(root + "/Katalog");

                    if (!myDir.exists()) {
                        myDir.mkdirs();
                    }

                    String time = String.valueOf((new Date()).getTime());
                    String name = time + ".jpg";
                    myDir = new File(myDir, name);
                    FileOutputStream out = new FileOutputStream(myDir);
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 100, out);

                    ScanFile(context, myDir);
                    out.flush();
                    out.close();

                    Toast.makeText(context, "Image berhasil di download...", Toast.LENGTH_LONG)
                            .show();

                } catch(Exception e){
                    e.printStackTrace();
                }
            }

            @Override
            public void onBitmapFailed(Drawable errorDrawable) {
                Toast.makeText(context, "Failed to Download Image", Toast.LENGTH_LONG);
            }

            @Override
            public void onPrepareLoad(Drawable placeHolderDrawable) {
                Toast.makeText(context, "Preparing to Download Image", Toast.LENGTH_LONG);
            }
        });
    }

    // validating email id
    public static boolean isValidEmail(String email) {
        String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

        Pattern pattern = Pattern.compile(EMAIL_PATTERN);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }

    // return string encrypted Base64
    public static String encodeBase64(String plain) {
        String encoded = Base64.encodeToString(plain.getBytes(), Base64.DEFAULT);
        return encoded.replaceAll("\\n", "");
    }

    // clear shared preferences
    public static void clearPref(SharedPreferences sharedPreferences) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.clear();
        editor.commit();
    }
    // get shared preferences
    public static SharedPreferences getSharedPreference(Context context, String preference) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(preference, Context.MODE_PRIVATE);
        return sharedPreferences;
    }

    // check Authenticated User
    public static boolean checkAuth(Context context) {
        SharedPreferences auth = getSharedPreference(context, Variable.USER_PREFERENCE);
        if(auth.contains(Variable.IS_AUTH) && auth.getBoolean(Variable.IS_AUTH, false)) return true;
        else return false;
    }

}
