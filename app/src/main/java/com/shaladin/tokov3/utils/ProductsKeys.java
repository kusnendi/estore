package com.shaladin.tokov3.utils;

/**
 * Created by EliteBook on 8/13/2016.
 */
public class ProductsKeys {
    public static final String KEY_PRODUCT_ID = "product_id";
    public static final String KEY_PRODUCT_NAME = "product";
    public static final String KEY_PRODUCT_PRICE = "price";
    public static final String KEY_PRODUCT_MAIN_PAIR = "main_pair";
    public static final String KEY_PRODUCT_MAIN_PAIR_DETAILED = "detailed";
    public static final String KEY_PRODUCT_IMAGE_PATH = "image_path";
    public static final String KEY_PRODUCT_STEP = "min_qty";
    public static final String KEY_PRODUCT_WEIGHT = "weight";
}
